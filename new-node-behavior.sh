#!/bin/bash

# Templates

htmlFile=$(cat << EOF
<link rel="import" href="../../bower_components/polymer-ts/polymer-ts.html">

<script src="{{nameDashes}}.js"></script>
EOF
)

tsFile=$(cat << EOF
/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts" />

/**
 * Requires:
 *  -
 */
class {{namePascal}} extends polymer.Base {
  //=================================================================
  // Declarations
  //=================================================================

  //


  //=================================================================


}
EOF
)

# Exit on any failure
set -e

# Default values
elementFolder='mto-node-behaviors'

# Check parameters
if [ -z "$1" ]; then
  >&2 echo -e "$0 BEHAVIOR_NAME [ELEMENT_FOLDER]\nExample:\n./new-node-behavior.sh mto-delete-behavior mto-editor"
  exit 1
fi

if [ -n "$2" ]; then
  elementFolder="$2"
fi

# Save parameters

nameDashes="$1"
namePascal="`printf $nameDashes | sed -e 's/\(-\|^\)\([a-z]\)/\U\2/g'`"

cd "app/elements/$elementFolder"

# Prepare templates
htmlFile=$(printf "$htmlFile" | sed "s/{{nameDashes}}/$nameDashes/g")
tsFile=$(printf "$tsFile" | sed "s/{{namePascal}}/$namePascal/g")

# Write them
printf "$htmlFile" > "${nameDashes}.html"
printf "$tsFile" > "${nameDashes}.ts"

# Import info
echo "To import:"
if [ "$elementFolder" = "mto-node-behaviors" ]; then
  echo "<link rel=\"import\" href=\"../$elementFolder/$nameDashes.html\">"
else
  echo "<link rel=\"import\" href=\"$nameDashes.html\">"
fi
echo ""
echo "@behavior($namePascal)"

/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

interface MtoInsertImageEvent extends Event {
  detail: {
    imageUrl: string
  };
}

/**
 * Form for inserting image via its URL. It fires mto-cancel and mto-insert-image
 * events.
 */
@component('mto-insert-image-form')
class MtoInsertImageForm extends polymer.Base {

  @property({ type: String, notify: true, value: '' })
  public imageUrl: string;

  @property({ type: Boolean, value: false })
  public noReset: boolean;

  public resetForm(): void {
    this.imageUrl = '';
  }

  private _onCancelTap() {
    this.fire('mto-cancel');

    if (!this.noReset) {
      this.resetForm();
    }
  }

  private _onInsertTap() {
    if (!this.isValid) return;
    this.fire('mto-insert-image', { imageUrl: this.imageUrl });

    if (!this.noReset) {
      this.resetForm();
    }
  }

  private _onKeyDown(event: KeyboardEvent): void {
    if (event.which === 13) {
      // Enter
      this._onInsertTap();
    }
  }

  @property({computed: 'imageUrl'})
  public isValid(imageUrl): boolean {
    return imageUrl ? true : false;
  }
}

MtoInsertImageForm.register();

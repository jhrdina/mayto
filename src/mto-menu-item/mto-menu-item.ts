/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

/**
 * Menu item for MtoMenu with icons support without paper-item's annoying
 * selection functionality.
 */
@component('mto-menu-item')
class MtoMenuItem extends polymer.Base
{
}

MtoMenuItem.register();

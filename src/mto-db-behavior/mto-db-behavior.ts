
/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

/**
 * Behavior for MtoEditor that handles first-login database initialization
 * or reparation.
 */
class MtoDbBehavior extends polymer.Base {

  nodes: MtoNodes;
  settings: any;

  @observe('nodesDbRef, settingsDbRef')
  dataChanged(nodesDbRef: IDBRef, settingsDbRef) {

    // After-login root node consistency check

    if (nodesDbRef && settingsDbRef) {
      if (!this.settings) {
       console.log('Missing settings');
       this.set('settings', {});
      }

      if (!this.settings.rootNodeId ||
          !this.nodes ||
          !this.nodes[this.settings.rootNodeId]) {
        var newId = nodesDbRef.push({
          text: 'Header'
        });

        this.set('settings.rootNodeId', newId);
      }

      if (!this.settings.lastOpenedNode ||
          !this.nodes ||
          !this.nodes[this.settings.lastOpenedNode]) {
        this.set('settings.lastOpenedNode', this.settings.rootNodeId);
      }
    }
  }
}

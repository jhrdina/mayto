/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

/**
 * Custom menu implementation (without paper-menu's annoying selection functionality)
 */
@component('mto-menu')
class MtoMenu extends polymer.Base
{
  @listen('click')
  _fireIronSelect() {
    this.fire('iron-select');
  }
}

MtoMenu.register();

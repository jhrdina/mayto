/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

/**
 * Static form with application's keyboard shortcuts
 */
@component('mto-shortcuts-view')
class MtoShortcutsView extends polymer.Base {

}

MtoShortcutsView.register();

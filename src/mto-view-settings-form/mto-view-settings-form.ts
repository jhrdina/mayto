/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

/**
 * Form that allows user to change the current view and its settings.
 */
@component('mto-view-settings-form')
class MtoViewSettingsForm extends polymer.Base {

  @property({ type: String, notify: true })
  viewMode: string;
}

MtoViewSettingsForm.register();

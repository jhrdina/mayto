/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

interface MtoAttachmentSelections {
  [index: number]: boolean;
}

/**
 * Thumbnail stripe for displaying images and videos thumbnails. It allows
 * selecting items and starting some operations (deleting, opening, ...).
 */
@component('mto-thumbnail-stripe')
@behavior(Polymer['IronResizableBehavior'])
class MtoThumbnailStripe extends polymer.Base {

  //=================================================================
  // Declarations
  //=================================================================

  // Polymer.IronResizableBehavior
  private notifyResize: () => void;

  //=================================================================

  public static supportedTypes = [
    'image',
    'youtube-video'
  ];

  @property({ type: Array, notify: true })
  public items: Array<MtoNodeParameter>;

  @property({ type: Boolean, value: false })
  public showScrollbars: boolean;

  private _rightArrowVisible;
  private _leftArrowVisible;

  // By how many pixels the view is scrolled
  private static SCROLL_STEP: number = 100;

  //=================================================================
  // Selections

  @property({ type: Object, notify: true, value: {} })
  public selections: MtoAttachmentSelections;

  private _deselectAll() {
    this.set('selections', {});
  }

  private _selectThumbnail(index: number) {
    var sel = {};
    sel[index] = true;
    this.selections = sel;
  }

  //=================================================================
  // TapSelections

  private _getIndexFromElement(element: any): number {
    return this.$.imagesRepeat.indexForElement(element);
  }

  private _onThumbnailMouseDown(event: Event) {
    var itemIndex = this._getIndexFromElement(event.target);

    if (!this.selections[itemIndex]) {
      event.preventDefault();
    }
  }

  private _onThumbnailFocus(event: Event) {
    var itemIndex = this._getIndexFromElement(event.target);

    if (!this.selections[itemIndex]) {
      // TODO: Handle holding Ctrl (multiselect)
      this._deselectAll();
      this._selectThumbnail(itemIndex);
    }
  }

  private _onThumbnailTap(event: Event): void {
    if (!this.selections[this._getIndexFromElement(event.target)]) {
      (<HTMLElement>event.currentTarget).focus();

    } else {
      // Already selected, firing thumbnail-open event
      var index = this.$.imagesRepeat.indexForElement(event.target);
      this.fire('mto-thumbnail-open', {
        index: index,
        item: this.items[index]
      });
    }

    event.stopPropagation();
  }

  private _getPreviousSupportedIndex(index: number) {
    for (let i = index - 1; i >= 0; --i) {
      if (
        this.items && this.items[i] && this.items[i].type &&
        MtoThumbnailStripe.supportedTypes.indexOf(this.items[i].type) !== -1
      ) {
        // Type is supported
        return i;
      }
    }
    // Not found
    return index;
  }

  private _getNextSupportedIndex(index: number) {
    for (let i = index + 1; i < this.items.length; ++i) {
      if (
        this.items && this.items[i] && this.items[i].type &&
        MtoThumbnailStripe.supportedTypes.indexOf(this.items[i].type) !== -1
      ) {
        // Type is supported
        return i;
      }
    }
    // Not found
    return index;
  }

  //=================================================================
  // KeySelections

  private _onThumbnailKeyDown(event: KeyboardEvent): void {

    var index = this._getIndexFromElement(event.target);

    switch (event.which) {
      case 39:
        // Right arrow
        var newIndex = this._getNextSupportedIndex(index);
        if (newIndex !== index) {
          this._selectThumbnail(newIndex);
        } else {
          return;
        }
        break;
      case 37:
        // Left arrow
        var newIndex = this._getPreviousSupportedIndex(index);
        if (newIndex !== index) {
          this._selectThumbnail(newIndex);
        } else {
          return;
        }
        break;
      case 13:
        // Enter
        this.fire('mto-thumbnail-open', {
          index: index,
          item: this.items[index]
        });
        break;
      case 8: // Backspace
      case 46: // Delete
        var newIndex = this._getPreviousSupportedIndex(index);
        if (newIndex === index) {
          newIndex = this._getNextSupportedIndex(index);
          newIndex = newIndex !== index ? newIndex - 1 : -1;
        }

        this.splice('items', index, 1);

        if (newIndex !== -1) {
          this._selectThumbnail(newIndex);
        }

        break;
      default:
        return;
    }

    event.preventDefault();
    event.stopPropagation();
  }

  //=================================================================
  // Event listeners

  public attached() {
    this.async(this.notifyResize, 1);
  }

  @listen('iron-resize')
  @listen('scrollBox.scroll')
  @listen('load')
  private _updateArrowsVisibility() {
    this._rightArrowVisible = this.$.scrollBox.scrollWidth - this.$.scrollBox.scrollLeft > this.$.scrollBox.clientWidth;
    this._leftArrowVisible = this.$.scrollBox.scrollLeft > 0;
  }

  private _moveLeft() {
    this.$.scrollBox.scrollLeft -= MtoThumbnailStripe.SCROLL_STEP;
  }

  private _moveRight() {
    this.$.scrollBox.scrollLeft += MtoThumbnailStripe.SCROLL_STEP;
  }

  //=================================================================
  // Focus on selection

  @observe('selections.*')
  private _onSelectionChanged(change) {
    var pathFragments = change.path.split('.');
    // if (pathFragments.length > 1 && pathFragments[1] !== this.nodeId) return;

    if (pathFragments.length === 1) {
      // Completely new selections object, focus the last item
      var indexes = Object.keys(this.selections);
      if (indexes.length > 0) {
        this.$.scrollBox.children[indexes[indexes.length - 1]].focus();
      }
    } else {
      // One item selection added or removed
      var index = parseInt(pathFragments[1]);
      if (this.selections[index]) {
        this.$.scrollBox.children[index].focus();
      }
    }
  }

  //=================================================================
  // Computed properties

  private _typeIsYoutube(type: string): boolean {
    return type === 'youtube-video';
  }

  private _typeIsImage(type: string): boolean {
    return type === 'image';
  }

  @property({computed: 'showScrollbars'})
  private _scrollbarsClass(showScrollbars): string {
    return showScrollbars ? 'scrollbars' : '';
  }

  private _computeSelectedClass(item, index, selectionsChange): string {
    return this.selections[index] ? 'selected' : '';
  }
}

MtoThumbnailStripe.register();

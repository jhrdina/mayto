/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

@component('mto-auth-buttons')
class MtoAuthButtons extends polymer.Base {

  @property({ type: String, notify: true, readOnly: true, value: 'buttons-view' })
  public mode: string;
  private _setMode: (mode: string) => void;

  @property({ type: Object })
  user: any;

  @observe('user')
  private userChanged(user) {
    this._setMode(user ? 'user-view' : 'buttons-view');
  }

  private _onUserTap() {
    this.fire('user');
  }

  private _onLogoutTap(e: Event) {
    this.fire('logout');
    e.stopPropagation();
  }

  private _onAuthTap() {
    this.fire('auth');
  }

  private _stopEvent(e) {
    e.stopPropagation();
  }
}

MtoAuthButtons.register();

/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

/**
 * Authentication form that can be toggled between sign-in and sign-up mode.
 * It fires "login" and "register" events with auth data. Busy and errorMessage
 * attributes can be used to indicate operation processing and show errors.
 */
@component('mto-auth-form')
@hostAttributes({ tabindex: '-1' })
class MtoAuthForm extends polymer.Base {

  @property({ type: String, notify: true, value: 'sign-in' })
  mode: string;

  @property({ type: Boolean, value: false })
  busy: boolean;

  @property({ type: String, notify: true })
  errorMessage: string;

  @property({ type: Object, value: function() {
    return this.parentNode;
  }})
  dialogElement: HTMLElement;

  passwordAgainInvalid: boolean;

  private _data: {
    type?: string,
    email?: string,
    password?: string,
    passwordAgain?: string
  } = {};

  public reset(): void {
    this._data = {};
  }

  private _registerClicked(): void {
    // TODO: Handle registration errors
    this.errorMessage = null;

    if (!this._data.password ||
        this._data.password !== this._data.passwordAgain) {
      return;
    }

    this.fire('register', {
      email: this._data.email,
      password: this._data.password
    });
  }

  private _loginClicked(): void {
    // TODO: Handle login errors
    this.errorMessage = null;

    this.fire('login', {
      type: 'password',
      email: this._data.email,
      password: this._data.password
    });
  }

  private _loginWithFacebookClicked(): void {
    this.errorMessage = null;

    this.fire('login', {
      type: 'facebook'
    });
  }

  private _loginWithGoogleClicked(): void {
    this.errorMessage = null;

    this.fire('login', {
      type: 'google'
    });
  }

  private _isSignUp(mode?: string): boolean {
    return this.mode === 'sign-up';
  }

  private _alreadyHaveAccountClicked(): void {
    this.errorMessage = null;

    this.set('mode', 'sign-in');
  }

  private _dontHaveAccountClicked(): void {
    this.errorMessage = null;

    this.set('mode', 'sign-up');
  }

  private _onKeyDown(event: KeyboardEvent): void {
    if (event.which === 13) {
      // Enter
      if (this._isSignUp()) {
        this._registerClicked();
      } else {
        this._loginClicked();
      }
    }
  }

  get _focusNode(): HTMLElement {
    return this.$.emailInput;
  }

  @listen('focus')
  private _focused(): void {
    this._focusNode.focus();
  }

  @observe('_data.password')
  @observe('_data.passwordAgain')
  passwordAgainChanged() {
      this.passwordAgainInvalid = this._data.password &&
                                  this._data.password !== this._data.passwordAgain;
  }
}

MtoAuthForm.register();

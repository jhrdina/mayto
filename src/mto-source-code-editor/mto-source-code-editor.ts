/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

@component('mto-source-code-editor')
class MtoSourceCodeEditor extends polymer.Base {

  @property({ type: String, notify: true, value: 'text' })
  public language: string;

  @property({ type: String, notify: true })
  public value: string;

  @property({ type: Boolean, value: false })
  public lineNumbers: boolean;

  attached() {
    // Ace editor stops bubbling of backspace and delete keys events, so we
    // capture them beforehand and fire custom bubbling event. This way
    // element predecessors can react to these keys.
    this.addEventListener('keydown', this._mtoSourceCodeBehavior_onKeyDownCapture, true);
  }

  detached() {
    this.removeEventListener('keydown', this._mtoSourceCodeBehavior_onKeyDownCapture, true);
  }

  private _mtoSourceCodeBehavior_onKeyDownCapture(event: KeyboardEvent) {
    switch (event.which) {
      case 8: // Backspace
      case 46: // Delete
        this.fire('mto-delete-key-event', { which: event.which });
        break;
    }
  }

  private _onEditorReady(event, detail): void {
    this.$.editorElement.editor.$blockScrolling = Infinity;
    this._lineNumbersChanged();
    this._valueChanged();
  }

  private _valueLock: boolean = false;

  @observe('value')
  private _valueChanged(): void {
    if (this._valueLock) return;
    this._valueLock = true;
    this.$.editorElement.value = this.value || '';
    this._valueLock = false;
  }

  private _onEditorContent(event, detail): void {
    if (this._valueLock) return;
    this._valueLock = true;
    this.value = detail.value;
    this._valueLock = false;
  }

  @observe('lineNumbers')
  private _lineNumbersChanged(): void {
    if (this.$.editorElement.editor) {
      this.$.editorElement.editor.renderer.setShowGutter(this.lineNumbers);
    }
  }

  @property({computed: '_selectedItem'})
  public _selectedLanguageText(item: HTMLElement): string {
    return item ? item.textContent.trim() : '';
  }

  public focus() {
    this.$.editorElement.focus();
  }
}

MtoSourceCodeEditor.register();

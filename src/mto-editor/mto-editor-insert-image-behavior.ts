/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

/**
 * MtoEditor behavior that handles image inserting.
 */
class MtoEditorInsertImageBehavior extends polymer.Base {

  //=================================================================
  // Declarations
  //=================================================================

  private nodes: MtoNodes;
  private selections: any;

  //=================================================================

  @listen('insertImageMenuItem.tap')
  private _openInsertImage() {
    var selectedNodeIds = Object.keys(this.selections);
    if (selectedNodeIds.length === 0) {
      this.$.selectNodeToast.open();
      return;
    }

    var dial = this.$.insertImageDialog;
    if (!dial) return;

    dial.open();
  }

  @listen('insertImageDialog.mto-cancel')
  private _onInsertImageCancel(): void {
    var dial = this.$.insertImageDialog;
    if (!dial) return;

    dial.close();
  }

  @listen('insertImageDialog.mto-insert-image')
  private _onInsertImage(event: MtoInsertImageEvent) {
    var selectedNodeIds = Object.keys(this.selections);
    if (selectedNodeIds.length > 0) {
      var node = this.nodes[selectedNodeIds[0]];
      if (!node.params) {
        this.set('nodes.' + selectedNodeIds[0] + '.params', []);
      }
      this.push('nodes.' + selectedNodeIds[0] + '.params', {
        type: 'image',
        url: event.detail.imageUrl
      });
    }

    // TODO: Save image URL
    var dial = this.$.insertImageDialog;
    if (!dial) return;
    dial.close();
  }
}

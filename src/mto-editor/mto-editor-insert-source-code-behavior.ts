/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts" />

/**
 * MtoEditor behavior that handles source code inserting
 * Requires:
 *  -
 */
class MtoEditorInsertSourceCodeBehavior extends polymer.Base {
  //=================================================================
  // Declarations
  //=================================================================

  private nodes: MtoNodes;
  private selections: any;

  //=================================================================

  @listen('insertSourceCodeMenuItem.tap')
  private _onInsertSourceCode() {
    // Check that there is a node selected
    var selectedNodeIds = Object.keys(this.selections);
    if (selectedNodeIds.length === 0) {
      this.$.selectNodeToast.open();
      return;
    }

    var node = this.nodes[selectedNodeIds[0]];
    if (!node.params) {
      this.set('nodes.' + selectedNodeIds[0] + '.params', []);
    }

    if (node.params.some((param) => {
      return param.type === 'source-code';
    })) {
      this.$.sourceCodeExistsToast.open();
      return;
    }

    this.push('nodes.' + selectedNodeIds[0] + '.params', <MtoSourceCodeParam>{
      type: 'source-code',
      language: 'text',
      value: ''
    });
  }
}

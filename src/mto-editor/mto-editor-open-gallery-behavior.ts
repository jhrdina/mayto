/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts" />

/**
 * MtoEditor behavior that handles gallery opening on mto-open-gallery signal.
 * Requires:
 *  -
 */
class MtoEditorOpenGalleryBehavior extends polymer.Base {
  //=================================================================
  // Declarations
  //=================================================================

  // Base
  private screen: string;

  //=================================================================

  private _galleryOpenedNodeId: string;
  private _galleryItemIndex: number;

  @listen('mto-open-gallery')
  private _openGalleryBehavior_onOpenGallery(event: Event, detail): void {
    this._galleryOpenedNodeId = detail.nodeId;
    this._galleryItemIndex = detail.index;
    this.screen = 'gallery';
  }

  @observe('screen')
  private _openGalleryBehavior_screenChanged(screen): void {
    if (screen === 'gallery') {
      this.$.gallery.focus();
    } else {
      // Clear last opened nodeId
      this._galleryOpenedNodeId = "";
    }
  }
}

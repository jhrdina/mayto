/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

/**
 * Fundamental element that wraps the whole application. Its functionality is
 * separated into several modules (behaviors).
 */
@component('mto-editor')
@behavior(MtoAuthBehavior)
@behavior(MtoDbBehavior)
@behavior(MtoEditorInsertImageBehavior)
@behavior(MtoEditorInsertSourceCodeBehavior)
@behavior(MtoEditorInsertVideoBehavior)
@behavior(MtoEditorOpenGalleryBehavior)
class MtoEditor extends polymer.Base {

  @property({ type: String, notify: true })
  route: string;

  screen: string = 'login';
  viewMode: string = 'bulletsView';
  nodes: MtoNodes;
  settings: any;

  private _openedNode: MtoNode;
  private _toolbarMode: string = 'main';

  @observe('nodes.*, settings.rootNodeId')
  private _openedNodeChanged(change): void {
    var pathFragments = change.path.split('.');
    if (pathFragments.length > 1 && pathFragments[1] !== this.settings.rootNodeId) return;

    if (this.nodes && this.settings && this.settings.rootNodeId) {
      this.set('_openedNode', this.nodes[this.settings.rootNodeId]);
    } else {
      this.set('_openedNode', null);
    }
  }

  @listen('shortcutsMenuItem.tap')
  private _shortcutsMenuItemTapped() {
    if (this.$.shortcutsDialog) {
      this.$.shortcutsDialog.open();
    }
  }

  @observe('_openedNode.text')
  private _openedNodeTextChanged(text) {
    if (this.$.editorNodeTitle) {
      this.$.editorNodeTitle.innerHTML = text;
    }
  }

  @observe('user')
  private _userChanged(user): void {
    if (user && this.screen === 'login') {
      this.screen = 'editor';
      this.viewMode = 'bulletsView';
    } else if (!user && this.screen !== 'login') {
      this.screen = 'login';
    }
  }

  @observe('route')
  private _routeChanged(route): void {
    switch (route) {
      case 'home':
      case 'login':
        this.screen = 'login';
        break;
      case 'bullets':
        this.screen = 'editor';
        this.viewMode = 'bulletsView';
        break;
      case 'table':
        this.screen = 'editor';
        this.viewMode = 'tableView';
        break;
      case 'gallery':
        this.screen = 'gallery';
        break;
    }
  }

  @observe('screen')
  @observe('viewMode')
  private _viewModeChanged(): void {
    // FIXME: Get rid of the startup warning
    if (this.screen === 'editor') {
      this.route = this.viewMode.replace(/View$/, '');
    } else {
      this.route = this.screen;
    }
  }
}

MtoEditor.register();

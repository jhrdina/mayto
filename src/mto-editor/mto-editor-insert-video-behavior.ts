/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

/**
 * MtoEditor behavior that handles videos inserting
 * Uses elements:
 *   #insertVideoMenuItem - menu item that causes InsertVideoDialog to be opened
 *   #insertVideoDialog
 *   #selectNodeToast - toast that is opened if there is no node selected
 */
class MtoEditorInsertVideoBehavior extends polymer.Base {

  //=================================================================
  // Declarations
  //=================================================================

  private nodes: MtoNodes;
  private selections: any;

  //=================================================================

  @listen('insertVideoMenuItem.tap')
  private _openInsertVideo() {
    var selectedNodeIds = Object.keys(this.selections);
    if (selectedNodeIds.length === 0) {
      this.$.selectNodeToast.open();
      return;
    }

    var dial = this.$.insertVideoDialog;
    if (!dial) return;

    dial.open();
  }

  @listen('insertVideoDialog.mto-cancel')
  private _onInsertVideoCancel(e: Event): void {
    var dial = this.$.insertVideoDialog;
    if (!dial) return;

    dial.close();
  }

  @listen('insertVideoDialog.mto-insert-video')
  private _onInsertVideo(event: MtoInsertVideoEvent) {
    // TODO: Simplify and DRY (ad. mto-editor-insert-image-behavior)
    var selectedNodeIds = Object.keys(this.selections);
    if (selectedNodeIds.length > 0) {
      var node = this.nodes[selectedNodeIds[0]];
      if (!node.params) {
        this.set('nodes.' + selectedNodeIds[0] + '.params', []);
      }
      this.push('nodes.' + selectedNodeIds[0] + '.params', {
        type: event.detail.service + '-video',
        url: event.detail.videoUrl
      });
    }

    var dial = this.$.insertVideoDialog;
    if (!dial) return;
    dial.close();
  }
}

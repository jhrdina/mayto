/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

/**
 * Form that allows users customize their profiles (e.g. nicknames)
 */
@component('mto-user-settings-form')
class MtoUserSettingsForm extends polymer.Base {

  @property({ type: String, notify: true, value: '' })
  public name: string;

  private _currentName: string;

  @observe('name')
  private _nameChanged(name): void {
    this._currentName = name;
  }

  private _onCancelTap() {
    this.fire('mto-cancel');
  }

  private _onOkTap() {
    if (!this.isValid) return;
    this.name = this._currentName;
    this.fire('mto-ok');
  }

  private _onKeyDown(event: KeyboardEvent): void {
    if (event.which === 13) {
      // Enter
      this._onOkTap();
    }
  }

  @property({computed: '_currentName'})
  public isValid(currentName: string): boolean {
    return currentName ? true : false;
  }
}

MtoUserSettingsForm.register();

/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts" />

interface MtoSourceCodeParam extends MtoNodeParameter {
  language: string;
  value: string;
}

/**
 * Node view behavior that handles source-code attachments
 * Requires:
 *  - MtoNodeRefBehavior
 */
class MtoSourceCodeBehavior extends polymer.Base {
  //=================================================================
  // Declarations
  //=================================================================

  // MtoNodeRefBehavior
  private node: MtoNode;

  //=================================================================

  private _mtoSourceCodeBehavior_data: MtoSourceCodeParam;
  private _mtoSourceCodeBehavior_index: number;

  @observe('node.params.*')
  private _nodeParamsChanged(change): void {
    if (this.node && this.node.params) {
      for (let i = 0; i < this.node.params.length; i++) {
          if (this.node.params[i].type === 'source-code') {
            this._mtoSourceCodeBehavior_index = i;
            this._mtoSourceCodeBehavior_data = <MtoSourceCodeParam>this.node.params[i];
            this.linkPaths('_mtoSourceCodeBehavior_data', 'node.params.' + i);
            return;
          }
      }
    }

    this._mtoSourceCodeBehavior_data = null;
    this._mtoSourceCodeBehavior_index = null;
  }

  private _mtoSourceCodeBehavior_onDeleteKeyEvent(event, detail) {
    switch (detail.which) {
      case 8: // Backspace
      case 46: // Delete
        // Delete empty code block
        if (this._mtoSourceCodeBehavior_data &&
            this._mtoSourceCodeBehavior_data.value === '') {
          this.splice('node.params', this._mtoSourceCodeBehavior_index, 1);
        }
        return;
    }
  }

  private _mtoSourceCodeBehavior_onKeyDown(event: KeyboardEvent) {
    event.stopPropagation();
  }
}

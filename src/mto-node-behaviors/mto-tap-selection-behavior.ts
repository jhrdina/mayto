/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts" />

/**
 * Node view behavior that handles node selecting using mouse or touch input.
 * Requires:
 *  - MtoSelectionBehavior
 *  - MtoFocusBehavior
 *  - MtoNodeRefBehavior
 */
class MtoTapSelectionBehavior extends polymer.Base {

  //=================================================================
  // Declarations
  //=================================================================

  // MtoSelectionBehavior
  public selected: boolean;
  public _deselectAll: () => void;
  public _setNodeSelection: (nodeId: string, selected: boolean) => void;

  // MtoNodeRefBehavior
  public nodeId: string;

  // MtoFocusBehavior
  public focused: boolean;

  //=================================================================
  // Deselecting
  //=================================================================

  public attached(): void {
    // this.listen(document, 'tap', '_mtoTapSelectionBehavior_docTapped');
  }

  public detached(): void {
    // this.unlisten(document, 'tap', '_mtoTapSelectionBehavior_docTapped');
  }

  private _mtoTapSelectionBehavior_docTapped(e: TouchEvent): void {
    // This event should not be reached (stopPropagation should be called),
    //   if you don't want all nodes to be deselected, e.g. you are selecting
    //   another node etc.

    if (!this.$.thisNode.contains(e.target)) {
      this._deselectAll();
    }
  }

  //=================================================================
  // Selecting
  //=================================================================

  @listen('thisNode.mousedown')
  private _mtoTapSelectionBehavior_onMouseDown(event: Event): void {
    // Prevent focus event triggered by mouse
    if (!this.selected) {
      event.preventDefault();
    }
  }

  @listen('mainText.tap')
  private _mtoTapSelectionBehavior_onTap(e): void {
    // console.log('SelectionTap');
    if (!this.selected) {
      this.focus();
    }
    e.stopPropagation();
  }

  @listen('thisNode.focus')
  private _mtoTapSelectionBehavior_onFocus(e): void {
    // console.log('SelectionFocus');
    if (!this.selected) {
      // TODO: Handle holding Ctrl (multiselect)
      this._deselectAll();
      this._setNodeSelection(this.nodeId, true);
    }
  }

  @observe('selected')
  private _mtoTapSelectionBehavior_selectedChanged(selected: boolean): void {
    // console.log('SelectionChanged', selected, this.selected);
    if (selected && !this.focused) {
      this.focus();
    }
  }
}

/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts" />

/**
 * Node view behavior that connects node text data model with edit field
 * in the way that changes from edit field are not propagated more frequently
 * than once in 1 second.
 *
 * Requires:
 *  - MtoNodeRefBehavior
 *  - main text element with ID "mainText"
 */
class MtoDebouncedMainTextBehavior extends polymer.Base {
  //=================================================================
  // Declarations
  //=================================================================

  // MtoNodeRefBehavior
  private nodeId: string;
  private node: MtoNode;
  private nodes: MtoNodes;

  //=================================================================
  private _updatingNodeText: boolean = false;

  @observe('node.text')
  private _debouncedMainTextBehavior_nodeTextChanged(nodeText, next): void {
    if (this._updatingNodeText) return;
    this.$.mainText.value = nodeText;
  }

  public _debouncedMainTextBehavior_fireUpdate(event): void {
    if (this.isDebouncerActive('node-update-' + this.nodeId)) {
      return;
    }

    this.debounce('node-update-' + this.nodeId, function() {
      if (this.nodes && this.nodes.hasOwnProperty(this.nodeId)) {
        this._updatingNodeText = true;
        this.set('node.text', this.$.mainText.value);
        this._updatingNodeText = false;
      }
    }, 1000);
  }
}

/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts" />

/**
 * Node view behavior that adds noMedia attribute. The attribute allows thumbnail-
 * stripe to be hidden.
 *
 * Requires:
 *  - MtoNodeRefBehavior
 */
class MtoNoMediaBehavior extends polymer.Base {

  //=================================================================
  // Declarations
  //=================================================================

  private node: MtoNode;

  //=================================================================

  @property({ type: Boolean, value: false })
  public noMedia: boolean;

  _mtoNoMediaBehavior_showMedia: boolean;

  //=================================================================

  @observe('node.params')
  private _mtoNoMediaBehavior_handleParamsUndefined(params) {
    if (!params) {
      this._mtoNoMediaBehavior_updateMediaVisibility({
        path: 'node.params',
        value: undefined
      });
    }
  }

  @observe('node.params.*, noMedia')
  private _mtoNoMediaBehavior_updateMediaVisibility(paramsChange, noMedia?: boolean): boolean {
    if (!this.node || !this.node.params || noMedia) {
      this._mtoNoMediaBehavior_showMedia = false;
      return;
    }

    var pathFragments = paramsChange.path.split('.');

    // Ignore params internal updates
    if (pathFragments.length > 3 && this._mtoNoMediaBehavior_showMedia !== undefined) return;

    // Find at least one supported type
    var found: boolean = false;
    for (let i = 0; i < this.node.params.length; i++) {
      if (MtoThumbnailStripe.supportedTypes.indexOf(this.node.params[i].type) !== -1) {
        found = true;
        break;
      };
    }

    this._mtoNoMediaBehavior_showMedia = found;

    // TODO: Handle type changes
    // TODO: Remember checked params to prevent type rechecks
  }
}

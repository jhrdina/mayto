/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts" />

interface MtoSelectEvent extends Event {
  detail: { direction: 'up' | 'down' };
  target: MtoKeySelectionBehavior;
}

/**
 * Node view behavior that handles selecting the node using keyboard arrows.
 * It allows simple node traversal configuration to fit view-mode's needs.
 *
 * Requires:
 *  - MtoNodeRefBehavior
 *  - MtoSelectionBehavior
 *  - MtoNoMediaBehavior
 */
class MtoKeySelectionBehavior extends polymer.Base {
  //=================================================================
  // Declarations
  //=================================================================

  // MtoNodeRefBehavior
  private node: MtoNode;
  private treeUtils: TreeUtils;
  private nodeId: string;
  private nodes: MtoNodes;

  // MtoSelectionBehavior
  private selections: MtoNodeSelections;
  private _deselectAll: () => void;
  private _setNodeSelection: (nodeId: string, selected: boolean) => void;
  private _setAttachmentSelection: (nodeId: string, attachmentIndex: number, selected: boolean) => void;

  // MtoNoMediaBehavior
  private noMedia: boolean;

  //=================================================================

  verticalTraversal: Array<string>;
  horizontalTraversal: Array<string>;
  targetSelectionMethods: {[target: string]: (nodeId: string) => boolean};

  ready() {
    this.verticalTraversal = [
      'node-above', 'node', 'attachment', 'node-below'
    ];

    this.horizontalTraversal = [];

    this.targetSelectionMethods = {
      'node-above': (nodeId: string): boolean => {
        var node: MtoNode = this.nodes[nodeId];
        if (!node) return false;

        if (!node.parentId) return false;
        var myPos = this.treeUtils.getPosition(nodeId);

        var idAbove: string;
        if (myPos <= 0) {
          // I'm the first child
          idAbove = node.parentId;
        } else {
          // I'm 2nd+ child
          var parent = this.nodes[node.parentId];
          if (!parent) return false;

          idAbove = this.treeUtils.getLastDescendant(parent.children[myPos - 1]);
        }

        // Select the node above
        this._deselectAll();
        this.selectionTraverse(idAbove, this.verticalTraversal, -1);

        return true;
      },

      'node-below': (nodeId: string): boolean => {
        var node: MtoNode = this.nodes[nodeId];
        if (!node) return false;

        var idBelow: string;
        if (node.children && node.children.length > 0) {
          // I have one or more children, focus the first one
          idBelow = node.children[0];

        } else {
          // I have no children, search in parents...

          var parent: MtoNode;
          var nodePos: number;
          while (node.parentId) {
            parent = this.nodes[node.parentId];
            if (!parent) return false;

            nodePos = this.treeUtils.getPosition(nodeId);
            if (nodePos < parent.children.length - 1) {
              idBelow = parent.children[nodePos + 1];
              break;
            }

            nodeId = node.parentId;
            node = parent;
          }

          if (!idBelow) return false;
        }

        this._deselectAll();
        this.selectionTraverse(idBelow, this.verticalTraversal, +1);

        return true;
      },

      'node': (nodeId: string): boolean => {
        this._deselectAll();
        this._setNodeSelection(nodeId, true);
        return true;
      },

      'attachment': (nodeId: string): boolean => {
        var node = this.nodes[nodeId];
        if (!node) return false;

        // Find the first attachment supported by ThumbnailStripe
        if (!node.params) return false;

        var paramIndex, found = false;
        for (paramIndex = 0; paramIndex < node.params.length; paramIndex++) {
          if (MtoThumbnailStripe.supportedTypes.indexOf(node.params[paramIndex].type) !== -1) {
            found = true;
            break;
          }
        }

        if (!found) return false;

        // Select thumbnail
        this._setAttachmentSelection(nodeId, paramIndex, true);
        return true;
      },

      // 'note': (nodeId: string): boolean => {
      //   if (neobsahuje poznamku) {
      //     return false;
      //   }
      //
      //   nastav vyber na poznamku;
      //   return true;
      // }
    }
  }

  private selectionTraverse(nodeId: string, traversalVector: Array<string>, delta: number, index?: number) {
    if (index === undefined) {
      if (delta < 0) {
        index = traversalVector.length - 2; // Minus TWO here!
      } else if (delta > 0) {
        index = 1;
      }
    }

    if (traversalVector.length === 0) {
      return;
    }

    // Crop index
    if (index > traversalVector.length - 1) {
      index = traversalVector.length - 1;
    } else if (index < 0) {
      index = 0;
    }

    // TODO: Handle missing selection method
    while (index >= 0 && index < traversalVector.length &&
           !this.targetSelectionMethods[traversalVector[index]](nodeId)) {
      index += delta;
    };
  }


  @listen('thisNode.keydown')
  private _mtoKeySelectionBehavior_onKeyDown(event: KeyboardEvent): void {
    var traversalIndex: number;

    switch (event.which) {

    case 38:
      // Up arrow
      traversalIndex = this.verticalTraversal.indexOf(this.selections[this.nodeId].target);
      this.selectionTraverse(this.nodeId, this.verticalTraversal, -1, traversalIndex - 1);
      break;

    case 40:
      // Down arrow
      traversalIndex = this.verticalTraversal.indexOf(this.selections[this.nodeId].target);
      this.selectionTraverse(this.nodeId, this.verticalTraversal, +1, traversalIndex + 1);
      break;

    case 39:
      // Right arrow
      traversalIndex = this.horizontalTraversal.indexOf(this.selections[this.nodeId].target);
      this.selectionTraverse(this.nodeId, this.horizontalTraversal, +1, traversalIndex + 1);
      break;

    case 37:
      // Left arrow
      traversalIndex = this.horizontalTraversal.indexOf(this.selections[this.nodeId].target);
      this.selectionTraverse(this.nodeId, this.horizontalTraversal, -1, traversalIndex - 1);
      break;

    default:
      return;
    }

    event.preventDefault();
  }
}

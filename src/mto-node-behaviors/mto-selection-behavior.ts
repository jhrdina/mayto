/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts" />

interface MtoNodeSelections {
  [nodeId: string]: MtoNodeSelectionDescriptor;
}

interface MtoNodeSelectionDescriptor {
  target: string; // 'node' or 'attachment'
  selected: boolean | MtoAttachmentSelections;
}

/**
 * Node view behavior that handles node selection basics
 */
class MtoSelectionBehavior extends polymer.Base {

  //=================================================================
  // Declarations
  //=================================================================

  public nodeId: string;

  //=================================================================

  @property({ type: Object, notify: true, value: {} })
  public selections: MtoNodeSelections;

  @property({ type: Boolean, value: false, notify: true })
  public selected: boolean;

  @observe('selections.*, nodeId')
  private _selectionsChanged(change): void {

    var pathFragments = change.path.split('.');
    if (pathFragments.length > 1 && pathFragments[1] !== this.nodeId) return;

    // console.log('Selected: ', this.selections[this.nodeId] || false);
    this.selected = (
      this.selections[this.nodeId] &&
      this.selections[this.nodeId].target === 'node' &&
      <boolean> this.selections[this.nodeId].selected
    );
  }

  public _setNodeSelection(nodeId: string, selected: boolean): void {
    this.set('selections.' + nodeId, <MtoNodeSelectionDescriptor> {
      target: 'node',
      selected: true
    });
  }

  public _setAttachmentSelection(nodeId: string, attachmentIndex: number, selected: boolean): void {
    // TODO: Select first visible attachment
    var selectionDescriptor: MtoNodeSelectionDescriptor = {
      target: 'attachment',
      selected: {}
    };
    selectionDescriptor.selected[attachmentIndex] = true;

    this.set('selections.' + nodeId, selectionDescriptor);
  }

  public _deselectAll(): void {
    this.set('selections', {});
  }
}

/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts" />

/**
 * Node view behavior that provides access to node's data-model.
 */
class MtoNodeRefBehavior extends polymer.Base {
  @property({ type: Object, notify: true })
  public nodes: Object;

  @property({ type: String })
  public nodeId: string;

  @property({ type: Object })
  public dbRef: IDBRef;

  public node: MtoNode;
  public treeUtils: TreeUtils;

  @observe('nodes, nodeId')
  private _modelChanged(nodes, nodeId): void {
    //FIXME: Throw error when nodes and nodeId is entered, but the node doesn't
    //       exist.
    this.node = nodes && nodeId ?
                this.nodes[this.nodeId] :
                null;

    this.unlinkPaths('node');
    this.linkPaths('node', 'nodes.' + nodeId);
  }

  @observe('nodes.*')
  private _nodeChanged(change): void {
    var pathFragments = change.path.split('.');
    if (pathFragments[1] !== this.nodeId) return;

    if (pathFragments.length === 2 &&
        this.node !== this.nodes[this.nodeId]) {
      this.node = this.nodes[this.nodeId];
    }
  }

  @observe('nodes, dbRef')
  private _treeUtilsChanged(nodes, dbRef): void {
    this.treeUtils = nodes && dbRef ?
                     new TreeUtils(this.nodes, this.dbRef) :
                     null;
  }
}

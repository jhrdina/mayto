/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts" />

/**
 * Node view behavior that handles keyboard focus management
 * Requires:
 *  - MtoNodeRefBehavior
 *  - MtoSelectionBehavior
 */
class MtoFocusBehavior extends polymer.Base {

  //=================================================================
  // Declarations
  //=================================================================

  // MtoNodeRefBehavior
  private nodeId: string;
  private node: MtoNode;
  private treeUtils: TreeUtils;

  // MtoSelectionBehavior
  private selected: boolean;

  //=================================================================

  public focused: boolean;

  public attached() {
    if (this.selected) {
      // TODO: create autofocus attribute for this (don't do this as default)
      // console.log('AttachedFocus');
      this.focus();
    }
  }

  public focus(): void {
    this.$.thisNode.focus();
  }

  @listen('thisNode.focus')
  private _onFocus(): void {
    this.focused = true;
  }

  @observe('selected')
  private _selectedChanged(selected: boolean): void {
    if (!selected) {
      this.focused = false;
    }
  }
}

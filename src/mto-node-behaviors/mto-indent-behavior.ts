/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts" />

class MtoIndentEvent extends Event {
  detail: { direction: 'left' | 'right' };
  target: MtoIndentBehavior;
}

/**
 * Node view behavior that handles indentation
 * Requires:
 *  - MtoNodeRefBehavior
 */
class MtoIndentBehavior extends polymer.Base {

  //=================================================================
  // Declarations
  //=================================================================

  // MtoNodeRefBehavior
  private nodeId: string;
  private node: MtoNode;
  private treeUtils: TreeUtils;

  //=================================================================

  @listen('mto-indent')
  private _mtoIndentBehavior_onIndent(e: MtoIndentEvent): void {
    if (e.target === this) return;

    var childIndex = this.treeUtils.getPosition(e.target.nodeId);

    switch (e.detail.direction) {
    case 'right':
      if (childIndex > 0) {
        this.treeUtils.move(e.target.nodeId, this.node.children[childIndex - 1]);
      }
      break;

    case 'left':
      if (this.node && this.node.parentId) {
        while (childIndex + 1 < this.node.children.length) {
            this.treeUtils.move(this.node.children[childIndex + 1], e.target.nodeId);
        }

        this.treeUtils.move(e.target.nodeId, this.node.parentId, this.treeUtils.getPosition(this.nodeId) + 1);
      }
      break;

    default:
      return;
    }

    e.stopPropagation();
  }

  @listen('thisNode.keydown')
  private _mtoIndentBehavior_onKeyDown(e: KeyboardEvent): void {
    switch (e.which) {
    case 9:
      // Tab
      if (e.shiftKey) {
        this.fire('mto-indent', { direction: 'left' });
      } else {
        this.fire('mto-indent', { direction: 'right' });
      }
      break;

    default:
      return;
    }

    e.preventDefault();
  }
}

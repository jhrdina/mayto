/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts" />

/**
 * Node view behavior that handles nodes deleting
 * Requires:
 *  - MtoNodeRefBehavior
 *  - MtoSelectionBehavior
 */
class MtoDeleteBehavior extends polymer.Base {
  //=================================================================
  // Declarations
  //=================================================================

  // MtoNodeRefBehavior
  private nodeId: string;
  private treeUtils: TreeUtils;
  private node: MtoNode;

  // MtoSelectionBehavior
  private _deselectAll: () => void;
  private _setNodeSelection: (nodeId: string, selected: boolean) => void;

  //=================================================================

  @listen('thisNode.keydown')
  private _mtoDeleteBehavior_onKeyDown(e: KeyboardEvent): void {
    switch (e.which) {
    case 8:
    case 46:
      // Backspace or Delete
      this.fire('mto-delete-child', { nodeId: this.nodeId });
      break;

    default:
      return;
    }

    e.preventDefault();
  }

  @listen('mto-delete-child')
  _mtoDeleteBehavior_delete(e) {
    if (e.target === this) return;

    var childIndex = this.treeUtils.getPosition(e.target.nodeId);

    this._deselectAll();
    if (childIndex <= 0) {
      // From my first child
      this._setNodeSelection(this.nodeId, true);
    } else {
      // From my 2nd+ child
      this._setNodeSelection(this.node.children[childIndex - 1], true);
    }

    this.treeUtils.delChild(this.nodeId, e.target.nodeId);

    e.stopPropagation();
  }
}

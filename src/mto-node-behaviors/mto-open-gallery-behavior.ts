/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts" />

/**
 * Listens for a 'mto-thumbnail-open' event (probably from thumbnailStripe element)
 * and fires 'mto-open-gallery' event with additional info (nodeId...)
 *
 * Requires:
 *  - NodeRefBehavior
 */
class MtoOpenGalleryBehavior extends polymer.Base {
  //=================================================================
  // Declarations
  //=================================================================

  // NodeRefBehavior
  private nodeId: string;

  //=================================================================

  @listen('mto-thumbnail-open')
  private _onThumbnailTap(event: Event, detail): void {
    this.fire('mto-open-gallery', {
      nodeId: this.nodeId,
      index: detail.index
    });

    event.stopPropagation();
  }
}

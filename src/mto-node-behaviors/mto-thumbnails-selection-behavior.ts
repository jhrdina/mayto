/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts" />

/**
 * Handles 2-way data-binding between global selections object
 * ('selections.<nodeId>.selected') and current node thumbnails selection object
 * ('stripeSelections').
 *
 * Requires:
 *  - $.thumbnailStripe: MtoThumbnailStripe element instance used for selections
 *                       data-binding
 *  - MtoSelectionBehavior
 *  - MtoNodeRefBehavior
 */
class MtoThumbnailsSelectionBehavior extends polymer.Base {
  //=================================================================
  // Declarations
  //=================================================================

  // MtoSelectionBehavior
  private selections: MtoNodeSelections;
  private _setAttachmentSelection: (nodeId: string, attachmentIndex: number, selected: boolean) => void;

  // MtoNodeRefBehavior
  private nodeId: string;

  //=================================================================

  private _mtoThumbnailSelectionBehavior_stripeSelections: MtoAttachmentSelections;

  //=================================================================

  private _mtoThumbnailSelectionBehavior_updatingSelections: boolean = false;

  /**
   * Waits for correct nodeId and 'attachment' as selection target and updates
   * thumbnails selections object. Otherwise clears thumbnails selection.
   */
  @observe('selections.*, nodeId')
  private _mtoThumbnailsSelectionBehavior_selectionsChanged(change): void {

    var pathFragments = change.path.split('.');
    if (pathFragments.length > 1 && pathFragments[1] !== this.nodeId) return;

    if (this._mtoThumbnailSelectionBehavior_updatingSelections ) {
      return;
    }

    if (
      this.selections[this.nodeId] &&
      this.selections[this.nodeId].target === 'attachment' &&
      this.selections[this.nodeId].selected
    ) {
      this._mtoThumbnailSelectionBehavior_stripeSelections =
        <MtoAttachmentSelections> this.selections[this.nodeId].selected;
    } else {
      this._mtoThumbnailSelectionBehavior_stripeSelections = {};
    }
  }

  @observe('_mtoThumbnailSelectionBehavior_stripeSelections.*')
  private _onThumbnailSelectionsChanged(detail: {value: MtoAttachmentSelections}): void {
    if (!detail.value || Object.keys(detail.value).length === 0) {
      return;
    }

    this._mtoThumbnailSelectionBehavior_updatingSelections = true;
    var newSelections: MtoNodeSelections = {};
    newSelections[this.nodeId] = {
      target: 'attachment',
      selected: detail.value
    };
    this.set('selections', newSelections);
    this._mtoThumbnailSelectionBehavior_updatingSelections = false;
  }
}

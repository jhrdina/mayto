/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts" />

/**
 * Node-view behavior that operates with edit mode and main text editing.
 * Requires:
 *  - MtoSelectionBehavior
 */
class MtoEditingBehavior extends polymer.Base {
  //=================================================================
  // Declarations
  //=================================================================

  // MtoSelectionBehavior
  private selected: boolean;

  //=================================================================

  @property({ type: Boolean, value: false, readOnly: true, notify: true })
  public editing: boolean;
  private _setEditing: (editing: boolean) => void;

  @listen('mainText.tap')
  private _mtoEditingBehavior_onTap(e: Event): void {
    // console.log('EditingTap', e);

    if (this.selected && !this.editing) {
      this._setEditing(true);
    }
  }

  @observe('selected')
  private _mtoEditingBehavior_selectedChanged(selected: boolean): void {
    if (!selected) {
      this._setEditing(false);
    }
  }

  @observe('editing')
  private _mtoEditingBehavior_editingChanged(editing: boolean): void {
    if (editing) {
      this.$.mainText.focus();
    }
  }

  //=================================================================
  // Keyboard Events
  //=================================================================

  @listen('thisNode.keypress')
  private _mtoEditingBehavior_onKeyPress(e: KeyboardEvent): void {
    if (e.target !== this.$.thisNode || e.which === 0 || e.which === 13 || e.ctrlKey || e.metaKey || e.altKey) {
      // 13 = Enter
      return;
    }

    // Go to edit mode after pressing any character key
    this._setEditing(true);
  }

  @listen('thisNode.keydown')
  private _mtoEditingBehavior_onKeyDown(e: KeyboardEvent): void {
    switch (e.which) {
    case 113:
      // F2
      this._setEditing(true);
      break;

    default:
      return;
    }

    e.preventDefault();
  }

  @listen('mainText.keydown')
  private _mtoEditingBehavior_onMainTextKeyDown(e: KeyboardEvent): void {
    e.stopPropagation();

    switch (e.which) {
    case 13:
      // Enter
      if (!e.ctrlKey && !e.shiftKey) {
        // Enter alone
        this._setEditing(false);
        this.$.thisNode.focus();
      } else {
        return;
      }
      break;

    case 27:
      // Escape
      // TODO: Don't save changes
      this._setEditing(false);
      this.$.thisNode.focus();
      break;

    default:
      return;
    }

    e.preventDefault();
  }
}

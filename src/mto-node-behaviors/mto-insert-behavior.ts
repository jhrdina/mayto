/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts" />

/**
 * Node view behavior that handles siblings and children inserting
 * Requires:
 *  - MtoNodeRefBehavior
 *  - MtoSelectionBehavior
 */
class MtoInsertBehavior extends polymer.Base {

  //=================================================================
  // Declarations
  //=================================================================

  // MtoNodeRefBehavior
  private nodeId: string;
  private node: MtoNode;
  private treeUtils: TreeUtils;

  // MtoSelectionBehavior
  private _deselectAll: () => void;
  private _setNodeSelection: (nodeId: string, selected: boolean) => void;

  //=================================================================

  @listen('thisNode.keydown')
  private _mtoInsertBehavior_onKeyDown(e: KeyboardEvent): void {
    switch (e.which) {
    case 13:
      // Enter alone
      if (e.ctrlKey) {
        this._addChild();
      } else {
        this.fire('mto-add-sibling', { direction: 'down' });
      }
      break;

    case 45:
      // Insert
      this._addChild();
      break;

    default:
      return;
    }

    e.preventDefault();
  }

  @listen('mto-add-sibling')
  private _addSibling(e): void {
    if (e.target === this) return;

    var nodeId = e.target.nodeId,
        node = e.target.node;

    if (e.detail.direction === 'down') {
      if (!node || !node.parentId) {
        return;
      }

      var insertPos = this.treeUtils.getPosition(nodeId) + 1;
      var newNodeId = this.treeUtils.addChild(node.parentId, { text: '' }, insertPos);

      this._deselectAll();
      this._setNodeSelection(newNodeId, true);
    }

    e.stopPropagation();
  }

  private _addChild(): void {
    var newNodeId = this.treeUtils.addChild(this.nodeId, { text: '' });
    // Focus inserted node
    this._deselectAll();
    this._setNodeSelection(this.node.children[this.node.children.length - 1], true);
  }
}

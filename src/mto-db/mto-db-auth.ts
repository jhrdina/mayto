/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

interface LoginParams {
  provider: string;
  email?: string;
  password?: string;
}

/**
 * User authentication management element. Merges public metadata with database
 * user info into a single "user" attribute. Allows users to login and create
 * account.
 */
@component('mto-db-auth')
class MtoDbAuth extends polymer.Base
{
   @property({ type: String })
   location: string;

   @property({ type: Object, notify: true })
   user: User;

   @property({ type: String, value: 'password'})
   provider: string;

   userMetadata: any;
   userMetadataLocation: string = null;

   @property({ type: Boolean, notify: true, readOnly: true })
   metadataReady: boolean;
   private _setMetadataReady: (metadataReady: boolean) => void;

   private _lockUserName: boolean = false;

   public login(params: LoginParams): void {
     this.$.auth.login(params);
   }

   public createUser(email: string, password: string): void {
     this.$.auth.createUser(email, password);

   }

   public logout() {
     this.$.auth.logout();
   }

   @listen('auth.logout')
   private _authLogout() {
     if (this.$.userMetadataDbDocument) {
       this.$.userMetadataDbDocument.disconnect();
     }
   }

   @observe('userMetadataDbRef')
   private _userMetadataChanged(userMetadataDbRef): void {
     this._setMetadataReady(userMetadataDbRef ? true : false);
   }

   @observe('metadataReady, user')
   private _userMetadataReadyChanged(ready, user: User) {
     if (ready && !this.userMetadata) {
       // Initialize user metadata
       this.set('userMetadata', {});
     }
   }

   public static getDefaultName(user: User): string {
     switch (user.provider) {
       case 'password':
         return user.password.email.replace(/@.*/, '');
       case 'facebook':
         return user.facebook.displayName;
       case 'google':
         return user.google.displayName;
       default:
         return '';
     }
   }

   @observe('user')
   private _userChanged() {
     this.set('userMetadataLocation',
       this.user ? 'https://maytoapp.firebaseio.com/users-metadata/' + this.user.uid : '');
   }

   @observe('user.name, userMetadata')
   private _userNameChanged(userName): void {
     if (!this._lockUserName && userName) {
       this.set('userMetadata.name', userName);
     }
   }

   @observe('userMetadata.name, user')
   userMetadataChanged(name, user) {
     if (user) {
       this._lockUserName = true;
       this.set('user.name', name);
       this._lockUserName = false;
     }
   }
   //TODO: add name to user when metadata loaded
}

MtoDbAuth.register();


/**
 * Interface for database-synchronized collection operations
 */
interface IDBRef {

  /**
   * Pushes a new item into the collection and returns its new key.
   * @return New unique key
   */
  push(object: Object, onComplete?: (error: any) => void): string;

  /**
   * Updates item with specified key
   */
  update(key: string, object: Object, onComplete?: (error: any) => void): void;

  /**
   * Removes item with specified key
   */
  remove(key: string, onComplete?: (error: any) => void): void;
}

/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts" />

/**
 * Element for single database collection data-binding. It connects to a collection
 * at "location" URL and provides its data in "data" attribute. Successful
 * connection can be detected using dbRef, which also provides an alterative
 * for data operations.
 */
@component('mto-db-document')
@behavior(MtoDbDocumentBehavior)
class MtoDBDocument extends polymer.Base {
  // Declarations from MtoDbDocumentBehavior
  public data: Object;
  public location: string;
  public dbRef: IDBRef;
  private _setDbRef: (dbRef: IDBRef) => void;

  // Current class
  _fbQuery: any;

  private _hasValue: boolean = false;
  private _dataLock: boolean = false;

  created() {
    this.linkPaths('data', '_fbData');
  }

  @observe('_fbQuery, _hasValue')
  private _dbRefChange(_fbQuery, _hasValue): void {
    this._setDbRef(_fbQuery && _hasValue ? new FirebaseDBRef(_fbQuery) : null);
  }

  // Detect disconnected state
  @observe('_fbQuery')
  private _fbQueryChanged(_fbQuery): void {
    if (!_fbQuery) {
      this._hasValue = false;
      this._setDbRef(null);
    }
  }

  // Called when firebase data are obtained
  private _onFirebaseValue(e): void {
    //console.log('Firebase value', e.detail.val());
    this._hasValue = true;
  }

  public disconnect(): void {
    this.$.fbDocument.disconnect();
  }
}

MtoDBDocument.register();

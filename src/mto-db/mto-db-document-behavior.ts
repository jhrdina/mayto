
/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

/**
 * Defines attributes and virtual methods for DBDocument objects, such as
 * MtoDBDocument or MtoDBMemoryDocument.
 */
class MtoDbDocumentBehavior extends polymer.Base {

  @property({ type: Object, notify: true })
  data: Object;

  @property({ type: String })
  location: string;

  @property({ type: Object, notify: true, readOnly: true })
  dbRef: IDBRef;
  _setDbRef: (dbRef: IDBRef) => void;

  disconnect(): void {
    // Virtual
  }
}


class MockDBRef implements IDBRef {

  public data: {[key: string]: any};
  public serverData: {[key: string]: any} = {};
  private _context: polymer.Base;

  constructor(data: {[key: string]: any}, context?: polymer.Base) {
    this.data = data;
    this.serverData = MockDBRef.deepClone(data);
    this._context = context || null;
  }

  public push(object: any, onComplete?: (error: any) => void): string {
    var key: string = MockDBRef.generatePushID();

    if (this._context) {
      this._context.set('data.' + key, object);
    } else {
      this.data[key] = object;
    }

    this.serverData[key] = MockDBRef.deepClone(object);

    return key;
  }

  public update(key: string, object: Object, onComplete?: (error: any) => void): void {
    if (this._context) {
      this._context.set('data.' + key, undefined);
      this._context.set('data.' + key, object);
    }

    delete this.serverData[key];

    this.serverData[key] = MockDBRef.deepClone(object);
  }

  public remove(key: string, onComplete?: (error: any) => void): void {
    delete this.data[key];
    delete this.serverData[key];
  }

  private static deepClone(obj: any): any {
    return JSON.parse(JSON.stringify(obj));
  }

  // ===========================================================================

  // Modeled after base64 web-safe chars, but ordered by ASCII.
  private static PUSH_CHARS = '-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz';

  // Timestamp of last push, used to prevent local collisions if you push twice in one ms.
  private static lastPushTime = 0;

  // We generate 72-bits of randomness which get turned into 12 characters and appended to the
  // timestamp to prevent collisions with other clients.  We store the last characters we
  // generated because in the event of a collision, we'll use those same characters except
  // "incremented" by one.
  private static lastRandChars = [];

  /**
   * Firebase ID generator from
   * https://gist.github.com/mikelehen/3596a30bd69384624c11
   */
  private static generatePushID(): string {
    var now = new Date().getTime();
    var duplicateTime = (now === MockDBRef.lastPushTime);
    MockDBRef.lastPushTime = now;

    var timeStampChars = new Array(8);
    for (var i = 7; i >= 0; i--) {
      timeStampChars[i] = MockDBRef.PUSH_CHARS.charAt(now % 64);
      // NOTE: Can't use << here because javascript will convert to int and lose the upper bits.
      now = Math.floor(now / 64);
    }
    if (now !== 0) throw new Error('We should have converted the entire timestamp.');

    var id = timeStampChars.join('');

    if (!duplicateTime) {
      for (i = 0; i < 12; i++) {
        MockDBRef.lastRandChars[i] = Math.floor(Math.random() * 64);
      }
    } else {
      // If the timestamp hasn't changed since last push, use the same random number, except incremented by 1.
      for (i = 11; i >= 0 && MockDBRef.lastRandChars[i] === 63; i--) {
        MockDBRef.lastRandChars[i] = 0;
      }
      MockDBRef.lastRandChars[i]++;
    }
    for (i = 0; i < 12; i++) {
      id += MockDBRef.PUSH_CHARS.charAt(MockDBRef.lastRandChars[i]);
    }
    if(id.length != 20) throw new Error('Length should be 20.');

    return id;
  }
}

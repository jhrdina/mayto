
/// <reference path="../../../../typings/chai/chai.d.ts" />
/// <reference path="../../../../typings/mocha/mocha.d.ts" />

var expect: Chai.ExpectStatic = expect || undefined;
var fixture = fixture || undefined;

suite('TreeUtils tests', function() {
  var treeUtils: TreeUtils, db: MockDBRef;

  setup(function() {
    db = new MockDBRef({
      '-a': {
        text: 'a',
        children: ['-aa', '-ab', '-ac']
      },
      '-aa': {
        parentId: '-a',
        text: 'aa',
        children: ['-aaa', '-aab']
      },
      '-aaa': {
        parentId: '-aa',
        text: 'aaa'
      },
      '-aab': {
        parentId: '-aa',
        text: 'aab'
      },
      '-ab': {
        parentId: '-a',
        text: 'ab'
      },
      '-ac': {
        parentId: '-a',
        text: 'ac',
        children: ['-aca', '-acb', '-acc']
      },
      '-aca': {
        parentId: '-ac',
        text: 'aca'
      },
      '-acb': {
        parentId: '-ac',
        text: 'acb'
      },
      '-acc': {
        parentId: '-ac',
        text: 'acc'
      }
    });

    treeUtils = new TreeUtils(db.data, db);
  });

  suite('addChild', function() {
    test('Adding to the existing node', function() {
      var newId = treeUtils.addChild('-a', {
        parentId: '-a',
        text: 'fourth text'
      });

      expect(newId).to.be.ok;
      expect(db.serverData['-a'].children).to.contain(newId);
      expect(db.serverData[newId].parentId).to.equal('-a');
    });

    test('Wrong parentId correction', function() {
      var newId = treeUtils.addChild('-a', {
        parentId: '-xxx',
        text: 'fourth text'
      });

      expect(newId).to.be.ok;
      expect(db.serverData[newId].parentId).to.equal('-a');
    });

    test('Missing parentId correction', function() {
      var newId = treeUtils.addChild('-a', {
        text: 'fourth text'
      });

      expect(newId).to.be.ok;
      expect(db.serverData[newId].parentId).to.equal('-a');
    });

    test('Parent does not exist thows error', function() {
      expect(function() {
        treeUtils.addChild('-xxx', {
          text: 'fourth text'
        });
      }).to.throw();
    });

    test('Insert at the end as default', function() {
      var newId = treeUtils.addChild('-a', {
        text: 'fourth text'
      });

      expect(db.serverData['-a'].children.indexOf(newId))
        .to.equal(db.serverData['-a'].children.length - 1);
    });

    test('Insert at the beginning', function() {
      var newId = treeUtils.addChild('-a', {
        text: 'fourth text'
      }, 0);

      expect(db.serverData['-a'].children.indexOf(newId)).to.equal(0);
    });

    test('Insert in the middle', function() {
      var newId = treeUtils.addChild('-a', {
        text: 'fourth text'
      }, 1);

      expect(db.serverData['-a'].children.indexOf(newId)).to.equal(1);
      expect(db.serverData['-a'].children.indexOf('-ab')).to.equal(2);
    });
  });

  suite('addParam', function() {
    test('New key value-pair adding', function() {
      treeUtils.addParam('-a', 'myKey', 'myValue');
      expect(db.serverData['-a'].params.myKey).to.equal('myValue');
    });
  });

  suite('delChild', function() {
    test('Deletes the child node', function() {
      expect(db.serverData).to.contain.keys('-aa');
      treeUtils.delChild('-a', '-aa');
      expect(db.serverData).to.not.contain.keys('-aa');
      expect(db.serverData['-a'].children).to.not.contain('-aa');
    });

    test('Deletes all child\'s subnodes recursively', function() {
      var len = Object.keys(db.serverData).length;
      treeUtils.delChild('-a', '-aa');
      expect(Object.keys(db.serverData).length).to.equal(len - 3);
      expect(db.serverData['-a'].children).to.not.contain('-aa');
    });

    test('Can handle missing parent->child reference', function() {
      db = new MockDBRef({
        '-a': {
          text: 'a',
          //children: ['-aa', '-ab']
        },
        '-aa': {
          parentId: '-a',
          text: 'aa'
        },
        '-ab': {
          parentId: '-a',
          text: 'ab'
        }
      });

      treeUtils = new TreeUtils(db.data, db);

      expect(db.serverData).to.contain.keys('-aa');
      treeUtils.delChild('-a', '-aa');
      expect(db.serverData).to.not.contain.keys('-aa');
    });
  });

  suite('delete', function() {
    test('Doesn\'t allow root node deletion', function() {
      treeUtils.delete('-a');
      expect(db.serverData['-a']).to.be.ok;
    });

    test('Deletes all subnodes recursively', function() {
      var len = Object.keys(db.serverData).length;
      treeUtils.delete('-aa');
      expect(Object.keys(db.serverData).length).to.equal(len - 3);
      expect(db.serverData['-a'].children).to.not.contain('-aa');
    });
  });

  suite('forEachParent', function() {
    test('Number of callback calls', function() {
      var counter = 0;
      treeUtils.forEachParent(db.data['-aab'], function () {
        counter++;
        return true;
      });
      expect(counter).to.equal(2);
    });

    test('Iteration order', function() {
      var counter = 0;
      treeUtils.forEachParent(db.data['-aab'], function (parentId) {
        counter++;
        switch(counter) {
          case 1:
          expect(parentId).to.equal('-aa');
          break;
          case 2:
          expect(parentId).to.equal('-a');
          break;
        }
        return true;
      });
    });

    test('Iteration stopping', function() {
      var counter = 0;
      treeUtils.forEachParent(db.data['-aab'], function () {
        counter++;
        if (counter === 1) {
          return false;
        }
        return true;
      });
      expect(counter).to.equal(1);
    });
  });

  suite('getLastDescendant', function() {
    test('Node without children', function() {
      expect(treeUtils.getLastDescendant('-aaa')).to.equal('-aaa');
    });

    test('Node with 1+ generations of descendants', function() {
      expect(treeUtils.getLastDescendant('-a')).to.equal('-acc');
    });
  });

  suite('getPosition', function() {
    test('Returns -1 when not found', function() {
      var pos = treeUtils.getPosition('-xxx');
      expect(pos).to.equal(-1);
    });

    test('Returns 0 for root node', function() {
      var pos = treeUtils.getPosition('-a');
      expect(pos).to.equal(0);
    });

    test('Returns position', function() {
      expect(treeUtils.getPosition('-aab')).to.equal(1);
    });
  });

  suite('hasChild', function() {
    test('Handles missing children array', function() {
      expect(treeUtils.hasChild(db.data['-ab'], '-xxx')).to.be.false;
    });
  });

  suite('move', function() {
    test('Removes link with the old parent', function() {
      treeUtils.move('-aaa', '-ab');
      expect(db.serverData['-aa'].children).to.not.contain('-aaa');
      expect(db.serverData['-aaa'].parentId).to.not.equal('-aa');
    });

    test('Creates link with the new parent', function() {
      treeUtils.move('-aaa', '-ab');
      expect(db.serverData['-ab'].children).to.contain('-aaa');
      expect(db.serverData['-aaa'].parentId).to.equal('-ab');
    });
  });
});

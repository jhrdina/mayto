/// <reference path="../../../typings/firebase/firebase.d.ts" />

/**
 * Implementation of the IDBRef for Firebase collection operations.
 */
class FirebaseDBRef implements IDBRef {

  private ref: Firebase;

  constructor(firebaseRef: Firebase) {
    this.ref = firebaseRef;
  }

  push(object: Object, onComplete?: (error: any) => void): string {
    var ref = this.ref.push(object, onComplete);
    return ref.key();
  }

  update(key: string, object: Object, onComplete?: (error: any) => void): void {
    this.ref.child(key).update(object, onComplete);
  }

  remove(key: string, onComplete?: (error: any) => void): void {
    this.ref.child(key).remove(onComplete);
  };
}

interface MtoNode {
  text: string;
  children?: Array<string>;
  parentId?: string;
  params?: Array<MtoNodeParameter>;
}

interface MtoNodeParameter {
  type: string;
}

interface MtoNodes {
  [key: string]: MtoNode;
}

/**
 * Advanced tree operations implemented using IDBRef interface
 */
class TreeUtils {

  nodes: Object;
  db: IDBRef;

  constructor(nodes: Object, db: IDBRef) {
    this.nodes = nodes;
    this.db = db;
  }

  private firstLoginInit(onComplete?: (error: any) => void): void {
    // Add first empty node
    var id = this.addNode({
      text: ''
    });

    // // Set it as rootNode and lastOpenedNode
    // return $firebase(userRef).$update({
    //   rootNodeId: newNodeId,
    //   lastOpenedNode: newNodeId

    // }).then(function() {
    //   rootNodeId = newNodeId;
    //   openedNodeId = newNodeId;

    //   return newNodeId;
    // });
  }

  private addNode(node: MtoNode, onComplete?: (error: any) => void): string {
    return this.db.push(node, onComplete);
  }

  //=================================================================
  // Searching
  //=================================================================

  public byId(nodeId: string): MtoNode {
    return this.nodes.hasOwnProperty(nodeId) ? this.nodes[nodeId] : null;
  }

  public idExists(nodeId: string): boolean {
    return this.byId(nodeId) !== null;
  }

  //=================================================================
  // Basic operations
  //=================================================================

  /**
   * Saves changes in node to the database.
   * @param  {Object} node Changed node object
   * @return {Promise}
   */
  public save(nodeId: string): void {
    // if (!this.isSynced(node)) {
    //   throw new Error('You cannot save node that is not in database.');
    // }
    return this.db.update(nodeId, this.byId(nodeId));
  }

  /**
   * Deletes node from database and from its parent.
   * @param  {Object} node Node to be deleted. When root node passed,
   *                       operation won't proceed.
   */
  public delete(nodeId: string): void {
    var node: MtoNode = this.byId(nodeId);

    if (!this.idExists(node.parentId)) {
      // Cannot delete root node
      return;
    } else {
      this.delChild(node.parentId, nodeId);
    }
  }

  //=================================================================
  // Children
  //=================================================================

  public hasChildren(node: MtoNode): boolean {
    return Array.isArray(node.children) && node.children.length > 0;
  }

  public hasChild(node: MtoNode, childId: string): boolean {
    return Array.isArray(node.children) &&
           node.children.indexOf(childId) !== -1;
  }

  public move(nodeId: string, newParentId: string, position?: number): void {
    // FIXME: What if moving to its own children?
    // TODO: Handle racing

    if (!this.idExists(newParentId)) {
      throw 'Target parent node doesn\'t exist: ' + newParentId;
    }

    var node = this.byId(nodeId);
    if (!node) {
      throw 'No node with such ID found: ' + nodeId;
    }

    // Remove child reference from its old parent
    var oldParent = this.byId(node.parentId);
    if (oldParent) {
      var index = oldParent.children.indexOf(nodeId);
      if (index > -1) {
        oldParent.children.splice(index, 1);
        this.save(node.parentId)
      } else {
        console.log("Invalid link: Old parent " + node.parentId + "doesn't link the node.")
      }
    } else {
      console.log("Invalid link: Old parent doesn't exist: " + node.parentId);
    }

    // Set new parent reference
    node.parentId = newParentId;
    this.save(nodeId);

    // Add reference New Parent --> Child
    this.addChildRef(newParentId, nodeId, position);
  }

  public addChildRef(parentId: string, childId: string, position?: number): void {
    var parent: MtoNode = this.byId(parentId);

    if (!this.hasChildren(parent)) {
      parent.children = [];
    }
    if (position !== undefined && position >= 0 && position <= parent.children.length) {
      // Insert at specified position
      parent.children.splice(position, 0, childId);
    } else {
      // Insert at the end
      parent.children.push(childId);
    }

    this.save(parentId);
  }

  public addChild(parentId: string, child: MtoNode, position?: number): string {

    var parent: MtoNode = this.byId(parentId);
    if (parent === null) {
      throw 'Parent node with id ' + parentId + ' was not found.';
    }

    // Add Node and reference Child --> Parent
    child.parentId = parentId;
    var newId = this.addNode(child);

    // Add reference Parent --> Child
    this.addChildRef(parentId, newId, position);

    return newId;
  }

  public delChild(parentId: string, childId: string): void {
    var parent: MtoNode = this.byId(parentId);
    if (!parent) {
      throw 'Parent node with id ' + parentId + ' was not found.';
    }

    // Recursively delete all its children
    var child: MtoNode = this.byId(childId);
    if (child && this.hasChildren(child)) {
      while (child.children && child.children.length > 0) {

        this.delChild(
          childId,
          child.children[child.children.length - 1]
        );

        // Get updated version of child
        child = this.byId(childId);
      }
    }

    // Remove parent->child reference
    if (parent.children) {
      var index = parent.children.indexOf(childId);
      if (index > -1) {
        parent.children.splice(index, 1);
      }
    }

    // Save to database
    this.db.update(parentId, parent);
    this.db.remove(childId);
  }

  public getPosition(nodeId: string): number {
    var node: MtoNode = this.byId(nodeId);
    if (!node) {
      return -1;
    }

    if (!node.parentId) {
      // Root node
      return 0;
    }

    var parent = this.byId(node.parentId);
    if (!parent) {
      // TODO: Auto-repair (fix reference)
      throw "Invalid link: Parent node with id " + node.parentId + " doesn't exist.";
    }

    if (!parent.children) {
      throw "Invalid link: Parent doesn't have children array";
    }

    return parent.children.indexOf(nodeId);
  }

  public getLastDescendant(nodeId: string): string {
    var node: MtoNode = this.byId(nodeId);
    if (!node) {
      throw 'Node with id ' + nodeId + ' was not found.';
    }

    if (this.hasChildren(node)) {
      return this.getLastDescendant(node.children[node.children.length - 1]);
    }

    return nodeId;
  }

  //=================================================================
  // Parameters
  //=================================================================

  public addParam(nodeId: string, key: string, value: any): void {
    var node = this.byId(nodeId);

    if (!node.params) {
      node.params = {};
    }
    node.params[key] = value;
    return this.save(nodeId);
  }

  public hasParam(node: MtoNode, key: string): boolean {
    return node.params && node.params.hasOwnProperty(key);
  }

  public delParam(nodeId: string, key: string): void {
    var node = this.byId(nodeId);
    delete node.params[key];
    return this.save(nodeId);
  }

  //=================================================================
  // Backend
  //=================================================================

  // isSynced(node: MtoNode): boolean {
  //   return node.$id !== undefined;
  // }

  //=================================================================
  // Multi-node operations
  //=================================================================

  public forEachParent(node: MtoNode, callback: (parentId: string) => boolean): void {
    while (node.parentId && callback(node.parentId)) {
      node = this.byId(node.parentId);
      if (!node) {
        break;
      }
    }
  }

  //=================================================================
  // Data integrity
  //=================================================================

  public isValidNode(object: any): boolean {
    return object.text && object.children && object.params;
  }
}

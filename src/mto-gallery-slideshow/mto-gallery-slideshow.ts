/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

/**
 * Animated gallery slideshow element with videos and images support that can
 * be navigated using displayed controls or using arrow keys.
 */
@component('mto-gallery-slideshow')
@hostAttributes({
  tabindex: '0'
})
class MtoGallerySlideshow extends polymer.Base {

  public static supportedTypes = [
    'image',
    'youtube-video'
  ];

  @property({ type: Array })
  public items: Array<MtoNodeParameter>;

  @property({ type: Number, value: 0, notify: true })
  public selected: number;

  private _entryAnimation: string;
  private _exitAnimation: string;

  //=================================================================
  // Navigation
  //=================================================================

  public goToNext(): void {
    var newSelected = this._getNextSupportedIndex(this.selected);
    if (newSelected !== this.selected) {
      this._entryAnimation = 'slide-from-right-animation';
      this._exitAnimation = 'slide-left-animation';
      this.selected = newSelected;
    }
  }

  public goToPrevious(): void {
    var newSelected = this._getPreviousSupportedIndex(this.selected);
    if (newSelected !== this.selected) {
      this._entryAnimation = 'slide-from-left-animation';
      this._exitAnimation = 'slide-right-animation';
      this.selected = newSelected;
    }
  }

  private _getPreviousSupportedIndex(index: number) {
    for (let i = index - 1; i >= 0; --i) {
      if (
        this.items && this.items[i] && this.items[i].type &&
        MtoGallerySlideshow.supportedTypes.indexOf(this.items[i].type) !== -1
      ) {
        // Type is supported
        return i;
      }
    }
    // Not found
    return index;
  }

  private _getNextSupportedIndex(index: number) {
    for (let i = index + 1; i < this.items.length; ++i) {
      if (
        this.items && this.items[i] && this.items[i].type &&
        MtoGallerySlideshow.supportedTypes.indexOf(this.items[i].type) !== -1
      ) {
        // Type is supported
        return i;
      }
    }
    // Not found
    return index;
  }

  //=================================================================
  // Event callbacks
  //=================================================================

  @listen('keydown')
  private _onKeyDown(event: KeyboardEvent): void {
    switch (event.which) {
      case 39:
        // Right arrow
        this.goToNext();
        break;
      case 37:
        // Left arrow
        this.goToPrevious();
        break;
      default:
        return;
    }

    event.preventDefault();
    event.stopPropagation();
  }

  //=================================================================
  // Computed properties
  //=================================================================

  @property({computed: 'selected, items.*'})
  private _previousButtonHidden(selected): boolean {
    return this._getPreviousSupportedIndex(selected) === selected;
  }

  @property({computed: 'selected, items.*'})
  private _nextButtonHidden(selected): boolean {
    return this._getNextSupportedIndex(selected) === selected;
  }

  /**
   * Return true if type is supported and 'type' matches 'value' or if
   * type is not supported and value is not specified.
   */
  private _typeIs(type: string, value?: string): boolean {
    if (!type) return false;
    if (MtoGallerySlideshow.supportedTypes.indexOf(type) !== -1) {
      return type === value;
    } else {
      return !value;
    }
  }
}

MtoGallerySlideshow.register();

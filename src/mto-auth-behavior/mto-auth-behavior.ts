/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

var page = page || undefined;

/**
 * Connects everything related to authentication.
 */
class MtoAuthBehavior extends polymer.Base {

  nodesLocation: string = null;
  settingsLocation: string = null;
  user: any;
  authBusy: boolean = false;
  authError: string = null;
  route: string;
  screen: string;

  @listen('authButtons.loginClicked')
  private _authBehavior_loginClicked() {
    console.log('Loooooogin.');
  }

  // Auth Buttons

  @listen('authButtons.user')
  private _authBehavior_authButtonsUser() {
    if (this.$.userSettingsDialog) {
      this.$.userSettingsDialog.open();
    }
  }

  @listen('authButtons.auth')
  private _authBehavior_authButtonsAuth() {
    this.route = 'login';
  }

  @listen('authButtons.logout')
  private _authBehavior_authButtonsLogout(): void {
    if (this.$.auth) {
      this.$.auth.logout();
    }
  }

  // Auth Form

  @listen('welcomeScreen.login')
  private _authBehavior_welcomeScreenLogin(e): void {
    if (!e.detail) {
      return;
    }

    var params: any = {};
    switch (e.detail.type) {
      case 'password':
        params.email = e.detail.email;
        params.password = e.detail.password;
        break;
      case 'facebook':
      case 'google':
        break;
      default:
        return;
    }

    if (!this.$.auth) return;
    this.$.auth.provider = e.detail.type;
    this.$.auth.login(params);

    this.authBusy = true;

    // TODO: Show spinner
  }

  @listen('welcomeScreen.register')
  private _authBehavior_welcomeScreenRegister(event, detail) {
    if (!detail || !this.$.auth) return;

    // FIXME: Handle email or password !== string
    this.$.auth.createUser(detail.email, detail.password);

    this.authBusy = true;
  }

  // Auth

  @listen('auth.user-created')
  private _authBehavior_authUserCreated() {
    this.authBusy = false;

    if (this.$.welcomeScreen) {
      this.$.welcomeScreen.resetAuthForm();
      this.$.welcomeScreen.authMode = 'sign-in';
    }

    if (this.$.accountCreatedToast) {
      this.$.accountCreatedToast.open();
    }

    // Close auth screen?
  }

  @listen('auth.login')
  private _authBehavior_authLogin() {
    this.authBusy = false;

    if (this.$.welcomeScreen) {
      this.$.welcomeScreen.resetAuthForm();
    }

    // Close auth screen?
  }

  @listen('auth.logout')
  private _authBehavior_authLogout() {
    this.authBusy = false;

    if (this.$.nodesDbDocument) {
      this.$.nodesDbDocument.disconnect();
    }

    if (this.$.settingsDbDocument) {
      this.$.settingsDbDocument.disconnect();
    }
  }

  @listen('auth.error')
  private _authBehavior_authError(e) {
    this.authBusy = false;
    this.authError = e.detail.message;
  }

  // User settings dialog
  @listen('userSettingsDialog.mto-cancel')
  @listen('userSettingsDialog.mto-ok')
  private _authBehavior_userSettingsClose() {
    if (this.$.userSettingsDialog) {
      this.$.userSettingsDialog.close();
    }
  }

  // User initialization

  @observe('userMetadataReady, user')
  private _authBehavior_detectEmptyNickname(ready, user: User) {
    if (ready && user && !user.name) {
      // Initialize user metadata with default name
      this.set('user.name', MtoDbAuth.getDefaultName(user));

      if (this.$.userSettingsDialog) {
        this.$.userSettingsDialog.open();
      }
    }
  }

  @observe('user')
  private _authBehavior_userChanged() {
    this.set('nodesLocation',
      this.user ? 'https://maytoapp.firebaseio.com/users-nodes/' + this.user.uid : '');

    this.set('settingsLocation',
      this.user ? 'https://maytoapp.firebaseio.com/users-settings/' + this.user.uid : '');
  }
}

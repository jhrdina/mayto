/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

interface MtoInsertVideoEvent extends Event {
  detail: {
    videoUrl: string,
    service: string
  };
}

/**
 * Form for inserting videos using their URL. It detects the service and lights
 * up its icon (little details matter). Right now it only supports YouTube
 * videos. Element fires mto-cancel and mto-insert-video events.
 */
@component('mto-insert-video-form')
class MtoInsertVideoForm extends polymer.Base {

  @property({ type: String, notify: true, value: '' })
  public videoUrl: string;

  @property({ type: String, notify: true, readOnly: true })
  public service: string;
  private _setService: (service: string) => void;

  @property({ type: Boolean, value: false })
  public noReset: boolean;

  public resetForm(): void {
    this.videoUrl = '';
    this.service = '';
  }

  private _onCancelTap() {
    this.fire('mto-cancel');

    if (!this.noReset) {
      this.resetForm();
    }
  }

  private _onInsertTap() {
    if (!this.urlIsValid) return;

    this.fire('mto-insert-video', {
      videoUrl: this.videoUrl,
      service: this.service
    });

    if (!this.noReset) {
      this.resetForm();
    }
  }

  private _onKeyDown(event: KeyboardEvent): void {
    if (event.which === 13) {
      // Enter
      this._onInsertTap();
    }
  }

  @property({computed: 'videoUrl'})
  public urlIsValid(videoUrl): boolean {
    var isValid = MtoYoutubeUtils.isVideoUrl(videoUrl);

    this._setService(isValid ? 'youtube' : null);
    // TODO: Add support for more services

    return isValid;
  }

  @property({computed: 'service'})
  public youTubeActiveClass(service): string {
    return service === 'youtube' ? 'active' : '';
  }

  @property({computed: 'service'})
  public vimeoActiveClass(service): string {
    return service === 'vimeo' ? 'active' : '';
  }
}

MtoInsertVideoForm.register();

/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

var pol: any = Polymer;

/**
 * Contenteditable text wrapper with databinding and formatting capabilities.
 * It allows you to limit the input using regExp and automatically removes
 * formatting when pasting from clipboard.
 */
@component('div-editable')
//@behavior(pol.IronControlState)
class DivEditable extends polymer.Base {

  @property({ type: String, value: "", notify: true })
  value: string;

  @property({ type: String, value: "" })
  regExp: string;

  @property({ type: Boolean, value: true })
  allowSpaces: boolean;

  @property({ type: String, value: null })
  placeholder: string;

  @property({ type: Boolean, value: false })
  selectOnFocus: boolean;

  @property({ type: Boolean, value: false, reflectToAttribute: true })
  disabled: boolean;

  valueLocked: boolean;

  valueString: string;

  @observe('value')
  @observe('placeholder')
  valueChanged(change) {
    if (!this.valueLocked) {
      this.$.container.innerHTML = this.value || this.placeholder;
    }
  }

  focus() {
    this.$.container.focus();
  }

  @listen('focus')
  onFocus() {
    if (this.selectOnFocus) {
      this.selectText();
    }

    if (!this.value) {
      this.$.container.innerText = '';
    }
  }

  selectText() {
    var range = document.createRange();
    range.selectNodeContents(this.$.container);
    var sel = window.getSelection();
    sel.removeAllRanges();
    sel.addRange(range);
  }

  @listen('blur')
  handleBlur() {
    window.getSelection().removeAllRanges();

    if (!this.value) {
      this.$.container.innerText = this.placeholder;
    }
  }

  @listen('keyup')
  handleKeyUp() {
    //on keyup change value
    //lock value to not binding back
    this.valueLocked = true;
    this.value = this.$.container.innerHTML || this.$.container.textContent || this.$.container.innerText || "";
    this.fire('change', { value: this.value });
    this.valueLocked = false;
  }

  @listen('keypress')
  handleKeyPress(e) {
    //filter by regular expression
    var regExp = new RegExp(this.regExp);
    e = e || window.event;
    var code = (typeof e.which != "undefined") ? e.which : e.button;
    var c = String.fromCharCode(code)

    if (e.charCode == 0) {
      return true;
    }

    if (c.match(regExp)) {
      return true;
    } else if (code == 32 && this.allowSpaces) { //space
      return true;
    }

    this.fire('invalid-input');
    e.preventDefault();
    return false;
  }

  @listen('paste')
  handlePaste(e) {
    //paste is not allowed

    e = e || window.event;

    // cancel paste
    e.preventDefault();

    // get text representation of clipboard
    var text = e.clipboardData.getData("text/plain");

    var regExp = new RegExp(this.regExp);
    if (!text.match(regExp)) {
      this.fire('invalid-input');
      return false;
    }

    // insert text manually
    document.execCommand("insertHTML", false, text);
  }
}

DivEditable.register();

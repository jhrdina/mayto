/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts" />

/**
 * Element for rendering a bullet. When active == true, bullet is permanently
 * highlighted.
 */
@component('mto-bullet')
class MtoBullet extends polymer.Base {

  @property({ type: Boolean, value: false })
  active: boolean;

  _computeBkgClass(active) {
    return 'bkg' + (this.active ? ' bkg-active' : '');
  }
}

MtoBullet.register();

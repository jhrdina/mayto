/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

/**
 * MtoDBDocument mock that works only in memory (doesn't actually contacts any
 * server)
 */
@component('mto-db-memory-document')
@behavior(MtoDbDocumentBehavior)
class MtoDbMemoryDocument extends polymer.Base {
  // Declarations from MtoDbDocumentBehavior
  data: Object;
  location: string;
  //dbRef: IDBRef;
  _setDbRef: (dbRef: IDBRef) => void;

  @property({ type: Object, notify: true })
  initData: Object;

  @observe('initData')
  private _initDataChanged(initData) {
    if (initData && this.data !== initData) {
      this.data = initData;
    }
  }

  @property({computed: 'data'})
  dbRef(data): IDBRef {
    return new MockDBRef(data, this);
  }
}

MtoDbMemoryDocument.register();

/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

/**
 * Simple toggle for selecting active view mode.
 */
@component('mto-view-mode-toggle')
class MtoViewModeToggle extends polymer.Base {

  @property({ type: String, notify: true })
  public selected: string;


}

MtoViewModeToggle.register();

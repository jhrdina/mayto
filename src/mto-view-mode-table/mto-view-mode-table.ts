/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

@component('mto-view-mode-table')
class MtoViewModeTable extends polymer.Base {

  @property({ type: Object, notify: true })
  public nodes: Object;

  @property({ type: Object })
  public dbRef: IDBRef;

  @property({ type: Object, notify: true })
  public settings: Object;

  @property({ type: Object, notify: true })
  public selections: any;
}

MtoViewModeTable.register();

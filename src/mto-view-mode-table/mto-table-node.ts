/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

@component('mto-table-node')
@behavior(MtoNodeRefBehavior)
@behavior(MtoSelectionBehavior)
@behavior(MtoTapSelectionBehavior)
@behavior(MtoKeySelectionBehavior)
@behavior(MtoEditingBehavior)
@behavior(MtoDeleteBehavior)
@behavior(MtoIndentBehavior)
@behavior(MtoDebouncedMainTextBehavior)
@behavior(MtoInsertBehavior)
@behavior(MtoFocusBehavior)
@behavior(MtoNoMediaBehavior)
@behavior(MtoOpenGalleryBehavior)
@behavior(MtoThumbnailsSelectionBehavior)
@behavior(MtoSourceCodeBehavior)
class MtoTableNode extends polymer.Base {

  //=================================================================
  // Declarations
  //=================================================================

  // MtoNodeRefBehavior
  private nodes: MtoNodes;
  private node: MtoNode;
  private treeUtils: TreeUtils;
  private nodeId: string;

  // MtoSelectionBehavior
  private _deselectAll: () => void;
  private _setNodeSelection: (nodeId: string, selected: boolean) => void;

  // MtoKeySelectionBehavior
  private verticalTraversal: Array<string>;
  private horizontalTraversal: Array<string>;
  private targetSelectionMethods: {[target: string]: (nodeId: string) => boolean};
  private selectionTraverse: (nodeId: string, traversalVector: Array<string>, delta: number, index?: number) => void;

  //=================================================================

  @property({ type: Boolean, value: false })
  public isHeadline: boolean;

  //=================================================================
  // Keyboard Selection
  //=================================================================

  public ready() {

    // Setup keyboard traversal
    this.verticalTraversal = ['node-above', 'node', 'attachment', 'node-below'];
    this.horizontalTraversal = ['node-left', 'node', 'node-right'];

    this.targetSelectionMethods['node-above'] = (nodeId: string): boolean => {
      var node: MtoNode = this.nodes[nodeId];
      if (!node || !node.parentId) return false;
      var parent = this.nodes[node.parentId];
      if (!parent) return false;

      var myPos = this.treeUtils.getPosition(nodeId);

      var idAbove: string;
      if (myPos > 0) {
        idAbove = parent.children[myPos - 1];
      } /*else if (this.settings.lastOpenedNode === node.parentId) {
        idAbove = node.parentId;
      }*/ else {
        return false;
      }

      // Select the node above
      this._deselectAll();
      this.selectionTraverse(idAbove, this.verticalTraversal, -1);

      return true;
    };

    this.targetSelectionMethods['node-below'] = (nodeId: string): boolean => {
      var node: MtoNode = this.nodes[nodeId];
      if (!node || !node.parentId) return false;
      var parent = this.nodes[node.parentId];
      if (!parent) return false;

      var myPos = this.treeUtils.getPosition(nodeId);

      var idBelow: string;
      if (myPos < parent.children.length - 1) {
        idBelow = parent.children[myPos + 1];
      } /*else if (this.settings.lastOpenedNode === nodeId && node.children) {
        idBelow = node.children[0];
      }*/ else {
        return false;
      }

      // Select the node below
      this._deselectAll();
      this.selectionTraverse(idBelow, this.verticalTraversal, 1);

      return true;
    };

    this.targetSelectionMethods['node-left'] = (nodeId: string): boolean => {
      var node: MtoNode = this.nodes[nodeId];
      if (!node || !node.parentId) return false;

      var idLeft: string = node.parentId;

      // Select the node on left
      this._deselectAll();
      this.selectionTraverse(idLeft, this.horizontalTraversal, -1);

      return true;
    };

    this.targetSelectionMethods['node-right'] = (nodeId: string): boolean => {
      var node: MtoNode = this.nodes[nodeId];
      if (!node || !node.children) return false;

      var idRight: string = node.children[0];

      // Select the node on left
      this._deselectAll();
      this.selectionTraverse(idRight, this.horizontalTraversal, 1);

      return true;
    };
  }

  //=================================================================
  // Computed classes
  //=================================================================

  @property({computed: 'selected'})
  private selectedClass(selected): string {
    return selected ? 'selected' : '';
  }

  @property({computed: 'isHeadline'})
  private headlineClass(isHeadline): string {
    return isHeadline ? 'headline' : '';
  }
}

MtoTableNode.register();

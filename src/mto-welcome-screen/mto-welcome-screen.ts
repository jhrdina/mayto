/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

/**
 * The greeting screen showed after the first start of the application. It simply
 * displays the app logo and allows user to login or create a new account.
 */
@component('mto-welcome-screen')
class MtoWelcomeScreen extends polymer.Base {

  @property({ type: String, notify: true, value: 'sign-in' })
  authMode: string;

  @property({ type: Boolean, value: false })
  authBusy: boolean;

  @property({ type: String, notify: true })
  authError: string;

  public resetAuthForm(): void {
    if (this.$.authForm) {
      this.$.authForm.reset();
    }
  }
}

MtoWelcomeScreen.register();

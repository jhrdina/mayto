
/// <reference path="../../../../typings/chai/chai.d.ts" />
/// <reference path="../../../../typings/mocha/mocha.d.ts" />

var expect: Chai.ExpectStatic = expect || undefined;
var fixture = fixture || undefined;

suite('mto-welcome-screen tests', function() {
  var el: MtoWelcomeScreen;
  var span: HTMLElement;

  setup(function() {
    el = fixture('basic');
  });

  test('Hello world as default', function() {
    span = el.$$('span');
    expect(span.textContent).to.equal('Hello world!');
  });

});

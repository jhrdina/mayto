/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

/**
 * YouTube video wrapper that allows inserting by whole video URL.
 */
@component('mto-youtube-video')
class MtoYoutubeVideo extends polymer.Base {

  @property({ type: String })
  public videoUrl: string;

  @property({computed: 'videoUrl'})
  private _videoId(videoUrl: string): string {
    return MtoYoutubeUtils.getVideoIdFromUrl(videoUrl);
  }
}

MtoYoutubeVideo.register();

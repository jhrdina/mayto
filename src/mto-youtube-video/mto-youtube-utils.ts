
interface MtoYoutubeVideoAttachment extends MtoNodeParameter {
  url: string;
}

/**
 * YouTube video shared utilities
 */
class MtoYoutubeUtils {
  public static getThumbnailUrl(videoId: string, minWidth: number): string {
    var _qualityString = '';

    if (!minWidth || minWidth <= 120) {
      _qualityString = '';
    } else if (minWidth <= 320) {
      _qualityString = 'mq';
    } else if (minWidth <= 480) {
      _qualityString = 'hq';
    } else if (minWidth <= 640) {
      _qualityString = 'sd';
    } else {
      _qualityString = 'maxres';
    }

    return 'http://img.youtube.com/vi/' + videoId + '/' + _qualityString + 'default.jpg';
  }

  // Source: http://stackoverflow.com/a/9102270
  private static _urlRegex: RegExp = /^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;

  public static getVideoIdFromUrl(url: string): string {
    if (!url) return null;
    var match = url.match(MtoYoutubeUtils._urlRegex);

    return match && match[2].length == 11 ? match[2] : null;
  }

  public static isVideoUrl(url: string): boolean {
    var match = url.match(MtoYoutubeUtils._urlRegex);
    return match ? true : false;
  }
}

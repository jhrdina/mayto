/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

/**
 * Element for fetching and rendering YouTube video thumbnail together with
 * play arrow. Successful load is indicated using "loaded" attribute. Minimal
 * thumbnail resolution can be set in the "minWidth".
 */
@component('mto-youtube-video-thumbnail')
class MtoYoutubeVideoThumbnail extends polymer.Base {

  @property({ type: String, value: 'Hello World!' })
  videoUrl: string;

  @property({ type: Boolean, notify: true, readOnly: true, value: false })
  public loaded: boolean;
  private _setLoaded: (loaded: boolean) => void;

  @property({ type: Number, value: 120 })
  public minWidth: number;

  @property({computed: 'videoUrl, minWidth'})
  private _imageSrc(videoUrl: string, minWidth: number): string {
    return MtoYoutubeUtils.getThumbnailUrl(
      MtoYoutubeUtils.getVideoIdFromUrl(videoUrl),
      minWidth
    );
  }

  private _onImageLoaded(event: Event, detail: {value: boolean}) {
    this._setLoaded(detail.value);
  }
}

MtoYoutubeVideoThumbnail.register();

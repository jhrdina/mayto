/// <reference path="../../../../typings/firebase/firebase.d.ts"/>
/// <reference path="../../../../typings/promise/promise.d.ts"/>

/*
 * Test Helpers ported to TypeScript from the official firebase-element tests
 * available at https://github.com/GoogleWebComponents/firebase-element/blob/master/test/test-helpers.html
 */

var firebaseTestProject = 'mto-db-firebase-demo';

function randomKey(): string {
  return (0|(Math.random() * 999999999)).toString();
}

function randomInt(min?: number, max?: number): number {
  min = min || 0;
  max = max || 10000;

  return Math.floor(Math.random() * (max - min)) + min;
}

function randomObject(min?: number, max?: number): any {
  return {
    value: randomInt(min, max)
  };
}

function waitForEvent(element: HTMLElement, event: string): Promise.IThenable<any> {
  return new Promise(function(resolve, reject) {
    element.addEventListener(event, function onEvent(e) {
      window.clearTimeout(timeout);
      element.removeEventListener(event, onEvent);
      resolve(e);
    });

    var timeout = window.setTimeout(function() {
      reject(new Error('Firebase response took more than 5 seconds.'));
    }, 5000);
  });
}

function firebaseUrl(project: string, path?: string): string {
  return 'https://' + project + '.firebaseio.com' + (path ? '/' + path : '');
}

function fixtureLocation(data: any): string {
  var firebase = new Firebase(firebaseUrl(firebaseTestProject));
  return firebaseUrl(
    firebaseTestProject,
    firebase.push(data || {}).key()
  );
}

function removeLocation(location: string): void {
  (new Firebase(location)).remove();
}

function fixtureFirebase(fixtureName: string, data: any): MtoDbFirebaseDocument {
  var firebase = fixture(fixtureName);
  firebase.location = fixtureLocation(data);
  return firebase;
}

function removeFirebase(firebase: MtoDbFirebaseDocument): void {
  removeLocation(firebase.location);
  firebase.disconnect();
}

function arrayOfPrimitives(length: number): Array<number> {
  var array = [];

  for (var i = 0; i < length; ++i) {
    array.push(randomInt());
  }

  return array;
}

function arrayOfObjects(length: number): Array<any> {
  var array = [];

  for (var i = 0; i < length; ++i) {
    array.push(randomObject());
  }

  return array;
}

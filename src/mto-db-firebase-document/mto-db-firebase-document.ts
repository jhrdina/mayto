/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>
/// <reference path="../../../typings/firebase/firebase.d.ts"/>

/*
 * Element ported to TypeScript from the official firebase-element and edited
 * to notify individual children changes (not the just the whole collection).
 *
 * Original sources available at:
 * https://github.com/GoogleWebComponents/firebase-element/blob/master/firebase-document.html
 */

/*
 * Copyright (c) 2015 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

interface MtoFirebaseChildEvent extends Event {
  detail: {
    childSnapshot: FirebaseDataSnapshot,
    oldChildSnapshot: FirebaseDataSnapshot
  };
}

@component('mto-db-firebase-document')
@behavior(Polymer['FirebaseQueryBehavior'])
class MtoDbFirebaseDocument extends polymer.Base {

  // Declarations inherited from FirebaseQueryBehavior
  private _applyRemoteDataChange: Function;
  public dataAsObject: any;
  public disconnect: () => void;
  public location: string;
  private _log: (message: string) => void;
  //=============================================

  @property({ type: Object, notify: true })
  public data: any;

  @property({
    type: Object,
    notify: true,
    computed: '_computeQuery(location)',
    observer: '_queryChanged'
  })
  public query: Firebase;

  private _computeQuery(location: string): Firebase {
    if (!location) {
      return;
    }
    return new Firebase(location);
  }

  @listen('firebase-child-added')
  @listen('firebase-child-changed')
  private _onFirebaseChildAdded(event: MtoFirebaseChildEvent): void {
    this._applyRemoteDataChange(function() {
      if (!this.data) {
        this.set('data', {});
      }
      this.set(
        ['data', event.detail.childSnapshot.key()],
        event.detail.childSnapshot.val());
    });
  }

  @listen('firebase-child-removed')
  private _onFirebaseChildRemoved(event: MtoFirebaseChildEvent): void {
    this._applyRemoteDataChange(function() {
      this.set(['data', event.detail.oldChildSnapshot.key()], null);
    });
  }

  _localDataChanged(change) {
    var pathFragments = change.path.split('.');

    if (pathFragments.length === 1) {
        this._updateRemoteDocument();
        return;
      }
      this._setRemoteDocumentChild(
        pathFragments[1],
        change.base[pathFragments[1]]
      );
  }

  _updateRemoteDocument() {
      this._log('Updating remote document');
      this.query.update(this.dataAsObject);
  }

  _setRemoteDocumentChild(key: string, value: any) {
    this.debounce('set-' + key, function() {
      this._log('Setting child "' + key + '" to', value);
      this.query.child(key).set(value);
    });
  }

  _removeRemoteDocumentChild(key: string) {
    this._log('Removing child "' + key + '"');
    this.query.child(key).remove();
  }

  @observe('location')
  private _locationChanged(location): void {
    if (!location) {
      this._applyRemoteDataChange(function() {
        this.set('data', null);
      });
    }
  }
}

MtoDbFirebaseDocument.register();

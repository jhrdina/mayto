/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts" />

/**
 * Special element for displaying static node without editing capabilities.
 */
@component('mto-gallery-text-node')
@behavior(MtoNodeRefBehavior)
class MtoGalleryTextNode extends polymer.Base {
  @observe('node.text')
  private _nodeTextChanged(nodeText): void {
    this.$.mainText.innerHTML = nodeText;
  }
}

MtoGalleryTextNode.register();

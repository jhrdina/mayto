/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts"/>

/**
 * Element attachment gallery screen for comfortable images and videos browsing.
 */
@component('mto-gallery')
@behavior(MtoNodeRefBehavior)
class MtoGallery extends polymer.Base {
  //=================================================================
  // Declarations
  //=================================================================

  private node: MtoNode;

  //=================================================================
  @property({ type: Number, notify: true })
  public selected: number;

  private _onGoBackTap() {
     window.history.back();
  }

  public focus() {
    if (this.$.slideshow) {
      this.$.slideshow.focus();
    }
  }

  @listen('keydown')
  private _onKeyDown(event: KeyboardEvent): void {
    switch (event.which) {
      case 27: // Escape
        this._onGoBackTap();
        break;
      default:
        return;
    }

    event.preventDefault();
    event.stopPropagation();
  }
}

MtoGallery.register();

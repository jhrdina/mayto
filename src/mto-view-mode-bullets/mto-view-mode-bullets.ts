/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts" />

/**
 * Bullet view main component.
 */
@component('mto-view-mode-bullets')
class MtoViewModeBullets extends polymer.Base {

  @property({ type: Object, notify: true })
  public nodes: Object;

  @property({ type: Object })
  public dbRef: IDBRef;

  @property({ type: Object, notify: true })
  public settings: Object;

  @property({ type: Object, notify: true })
  public selections: any;

  private treeUtils: TreeUtils;

  @observe('nodes, dbRef')
  _treeUtilsChanged(nodes, dbRef): void {
    this.treeUtils = nodes && dbRef ?
                     new TreeUtils(this.nodes, this.dbRef) :
                     null;
  }
}

MtoViewModeBullets.register();

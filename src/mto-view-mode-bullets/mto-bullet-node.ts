/// <reference path="../../bower_components/polymer-ts/polymer-ts.d.ts" />

/**
 * Component for displaying bullet node in bullet view. It uses several different
 * shared node behaviors that can provide and share properties and attributes.
 */
@component('mto-bullet-node')
@behavior(MtoNodeRefBehavior)
@behavior(MtoSelectionBehavior)
@behavior(MtoTapSelectionBehavior)
@behavior(MtoKeySelectionBehavior)
@behavior(MtoEditingBehavior)
@behavior(MtoDeleteBehavior)
@behavior(MtoIndentBehavior)
@behavior(MtoDebouncedMainTextBehavior)
@behavior(MtoInsertBehavior)
@behavior(MtoFocusBehavior)
@behavior(MtoNoMediaBehavior)
@behavior(MtoOpenGalleryBehavior)
@behavior(MtoThumbnailsSelectionBehavior)
@behavior(MtoSourceCodeBehavior)
class MtoBulletNode extends polymer.Base {

  //=================================================================
  // Declarations
  //=================================================================

  // MtoNodeRefBehavior
  public node: MtoNode;
  public treeUtils: TreeUtils;

  //=================================================================

  @property({ type: Boolean, value: false })
  public isHeadline: boolean;

  @property({ type: Boolean, value: false })
  public singleNode: boolean;

  //=================================================================
  // Computed classes
  //=================================================================

  @property({computed: 'selected'})
  private selectedClass(selected): string {
    return selected ? 'selected' : '';
  }

  @property({computed: 'isHeadline'})
  private headlineClass(isHeadline): string {
    return isHeadline ? 'headline' : '';
  }

  //=================================================================
  // Node operations
  //=================================================================

  hasChildren(): boolean {
    return this.treeUtils.hasChildren(this.node);
  }
}

MtoBulletNode.register();
